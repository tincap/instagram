<?php

use yii\db\Migration;

/**
 * Handles the creation of table `standard_account`.
 */
class m171026_114817_create_standard_account_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('account-standard', [
            'id'              => $this->primaryKey(),
            'username'        => $this->string(40)->notNull()->unique(),
            'password'        => $this->string(40)->notNull(),
            'proxy_id'        => $this->integer()->unsigned(),
            'email_id'        => $this->integer()->unsigned(),
            'working'         => $this->smallInteger(1),
            'banned'          => $this->smallInteger(1),
            'author'          => $this->string(12)->notNull(),
            'zaliv_name'      => $this->string(20),
            'cleared'         => $this->smallInteger(1)->defaultValue(0),
            'biographed'      => $this->smallInteger(1)->defaultValue(0),
            'photo'           => $this->smallInteger(1)->defaultValue(0),
            'public'          => $this->smallInteger(1)->defaultValue(0),
            'avatar'          => $this->smallInteger(1)->defaultValue(0),
            'filled'          => $this->smallInteger(1)->defaultValue(0),
            'error_type'      => $this->string(20),
            'follow_count'    => $this->integer()->unsigned()->defaultValue(0),
            'follower_count'  => $this->integer()->unsigned()->defaultValue(0),
            'following_count' => $this->integer()->unsigned()->defaultValue(0),
            'media_count'     => $this->integer()->unsigned(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        return false;
    }
}
