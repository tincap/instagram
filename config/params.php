<?php

$smsServices = require(__DIR__ . '/sms_services.php');

return [
    'anti_captcha' => [
        'api_key' => '3258fcd627d6169c34f882cc39314533',
    ],

    'mail' => [
        'host' => 'https://m.mail.ru/',
    ],

    'sms_services_keys' => $smsServices,

    // Чей sms-service будет выполнять действия
    'sms_services_main_author' => 'rinat',

    'mobile_proxy' => false,

    'photo_count' => 6,

    'photo_size' => 640,

    'border' => '%border%',
];