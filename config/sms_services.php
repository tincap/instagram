<?php

return [
    'sms_activate' => [
        'bulat'  => 'f67850b567fdffAbfe2fA1ef05b90e97',
        'sharaf' => '9c5d15eB930d0f9c50c5295f4dB0e695',
        'rinat'  => 'd63A78edc239dd8cd64fe106bAc08756',
        'ilnar'  => 'b6e87904bb5135171A059f45e83f2465',
    ],
    'online_sim' => [
        'bulat'  => 'e95613091f52f2f0b0259e1f5f572356',
        'sharaf' => 'e95613091f52f2f0b0259e1f5f572356',
        'rinat'  => 'e95613091f52f2f0b0259e1f5f572356',
        'ilnar'  => '841407246c9718dfd533ffc5e4b33a7c',
    ],
    'sim_sms' => [
        'bulat'  => '1IMFVCXFk8zJxpRhT9QNEX2K9mI6IU',
        'sharaf' => '1IMFVCXFk8zJxpRhT9QNEX2K9mI6IU',
        'rinat'  => '1IMFVCXFk8zJxpRhT9QNEX2K9mI6IU',
        'ilnar'  => '1IMFVCXFk8zJxpRhT9QNEX2K9mI6IU',
    ],
];