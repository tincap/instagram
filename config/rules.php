<?php

return [

    '/db/go/<actionType>' => '/db/go',

    'api/account/follow/<accountId>/<userId>/<mobileProxyId>' => 'api/account/follow',
    'api/account/set-follow-count/<accountId>/<followCount>' => 'api/account/set-follow-count',
    'api/account/set-working/<accountId>/<working>' => 'api/account/set-working',
    'api/account/set-clear/<accountId>/<clear>' => 'api/account/set-clear',
    'api/account/login/<accountId>/<mobileProxy>/<forceLogin>' => 'api/account/login',

    'api/settings/photo/<accountId>/<folderId>' => 'api/settings/photo',

    'api/<controller:(account|settings|media)>/<action:([\w-]*)>/<accountId:\d+>' => 'api/<controller>/<action>',

    'api/proxy/mobile-proxy-update/<mobileProxyId>' => 'api/proxy/mobile-proxy-update',

    'api/checkpoint-sms/fix/<accountId>/<phoneId>/<phoneNumber>/<smsService>/<smsNumber>' => 'api/checkpoint-sms/fix/',

    'api/checkpoint/fix/<accountId>' => 'api/checkpoint/fix/',

    'api/media/get-comment-list/<accountId>/<mediaId>' => 'api/media/get-comment-list/',
    'api/media/delete-comment/<accountId>/<mediaId>/<commentId>' => 'api/media/delete-comment/',
    'api/media/delete/<accountId>/<mediaId>' => 'api/media/delete/',
    'api/media/upload-photo/<accountId>/<photoId>' => 'api/media/upload-photo/',

    'api/checkpoint-sms/get-number/<smsService>/<country>' => 'api/checkpoint-sms/get-number/',

    'ajax/mobile-proxy/update/<mobileProxyId>' => 'ajax/mobile-proxy/update/',

    'ajax/mobile-proxy/discover-ip/<mobileProxyId>' => 'ajax/mobile-proxy/discover-ip/',
];