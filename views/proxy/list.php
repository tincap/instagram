<?php

/* @var $this yii\web\View */
/* @var $model \app\models\forms\proxy\AddForm */
/* @var $mobileProxyList array */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\web\View;

$this->title = 'Прокси';

$this->registerJsFile('@web/js/static/proxy/list.js', ['position' => View::POS_END]);
$this->registerCssFile('@web/css/static/proxy/list.css');

?>

<div style="float:left">
<h3 class="title">Прокси</h3>
</div>
<div style="float:right; margin-top:17px">

    <button style="display:none" class="btn btn-success has-spinner" id="update_proxy">
        <span class="spinner"><i class="glyphicon glyphicon-spin glyphicon-refresh"></i></span>
        Обновить прокси
    </button>
</div>

    <table class="table table-hover">

        <thead class="thead-inverse">
        <tr>
            <th>#</th>
            <th>Прокси</th>
            <th>IP</th>
            <th>Ссылка для обновления прокси вручную</th>
        </tr>
        </thead>

        <tbody>

        <?php
            foreach ($mobileProxyList as $mobileProxy) {
                /** @var $mobileProxy \app\models\Instagram\MobileProxy */
                ?>

                <tr>
                    <td width="50px" class="id" id="<?= $mobileProxy->id ?>"><?= $mobileProxy->id ?></td>
                    <td width="300px">
                        <?= $mobileProxy->ip ?>
                    </td>
                    <td width="150px" id="mobile_proxy_<?= $mobileProxy->id ?>"></td>
                    <td><a href="<?= $mobileProxy->update_url ?>" target="_blank">Обновить прокси #<?= $mobileProxy->id ?></a></td>
                </tr>

                <?php
            }
        ?>

        </tbody>
    </table>

<?php

$form = ActiveForm::begin() ?>

<i class="text-muted">ip:port|username:password|update_url</i>
<?= $form->field($model, 'proxyList')->textarea(['rows' => 6])->label(false) ?>

<?= Html::submitButton('Добавить', ['class' => 'btn btn-primary']) ?>

<?php ActiveForm::end() ?>
