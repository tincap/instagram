<?php

/* @var $this yii\web\View */
/* @var $userlist array */

$this->title = 'Управление пользователями';

?>

<h3 class="title">Управление пользователями</h3>

<p>
    <h5>Удаляет все нерабочие аккаунты</h5>
    <a href="/db/go/<?= \app\models\forms\DbForm::DELETE_UNWORKING ?>" class="btn btn-warning">Удалить нерабочие аккаунты</a>
</p>

<p style="margin-top: 25px">
    <h5>Удаляет все забаненные аккаунты</h5>
    <a href="/db/go/<?= \app\models\forms\DbForm::DELETE_BANNED ?>" class="btn btn-danger">Удалить забаненные аккаунты</a>
</p>

<p style="margin-top: 25px">
    <h5>photo = 1</h5>
    <a href="/db/go/<?= \app\models\forms\DbForm::UPDATE_PHOTO ?>" class="btn btn-success">Отметить все аккаунты заполненными фотками</a>
</p>