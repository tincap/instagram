<?php

/* @var $this yii\web\View */
/* @var $userlist array */
/* @var $model \app\models\forms\ChooseUserForm */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

$this->title = 'Выбор базы';

?>

<div class="settings-choose-user">

    <ul id="myTab" class="nav nav-tabs">
        <li class="active"><a href="#add" data-toggle="tab">Добавить новую базу</a></li>
        <li class=""><a href="#choose" data-toggle="tab">Выбрать базу</a></li>
    </ul>

    <div id="myTabContent" class="tab-content">

        <div class="tab-pane fade active in" id="add">

            <h3>Добавьте новую базу</h3>

            <?php

            $form = ActiveForm::begin([
                'id' => 'add-new-user-form',
                'layout' => 'inline',
                'options' => [],
            ]) ?>
            <?= $form->field($model, 'newUser')->textInput()->input('text', ['placeholder' => "Название базы"])->label(false) ?>

            <?= Html::submitButton('Добавить', ['class' => 'btn btn-default']) ?>
            <?php ActiveForm::end() ?>

        </div>


        <div class="tab-pane fade" id="choose">

            <h3>Выберите нужную базу</h3>

            <?php

            $form = ActiveForm::begin([
                'id' => 'choose-user-form',
                'options' => [],
            ]);

            $model->user = \app\models\App\User::getActiveUser();

            ?>
            <?= $form->field($model, 'user')->radioList($userlist)->label(false) ?>

            <?= Html::submitButton('Выбрать', ['class' => 'btn btn-default']) ?>
            <?php ActiveForm::end() ?>
        </div>
    </div>


</div>
