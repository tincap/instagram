<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\models\App\User;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => User::getActiveUser() . ' | <span class="text-success">' . \app\models\Instagram\Account::find()->where('working = 1')->count("*") . '</span> | <span class="text-warning">' . \app\models\Instagram\Account::find()->where('working = 0')->count("*") . '</span>',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Добавить новый список', 'url' => ['/add/standard']],

            [
                'label' => 'Настройки',
                'items' => [
                    ['label' => 'Управление аккаунтами', 'url' => '/db/settings'],
                    ['label' => 'Файлы для заполнения', 'url' => '/control-custom-files/file-list'],
                    ['label' => 'Выбор и добавление базы', 'url' => '/app/choose-user'],
                    ['label' => 'Мобильные прокси', 'url' => '/proxy/list'],
                ],
            ],

            [
                'label' => 'Выгрузка',
                'items' => [
                    '<li class="divider"></li>',
                    '<li class="dropdown-header">Тип выгрузки</li>',
                    ['label' => 'Standard', 'url' => '/account-list/standard'],
                    ['label' => 'SocialKit', 'url' => '/account-list/socialkit'],
                    ['label' => 'Instap', 'url' => '/account-list/instap'],
                    '<li class="divider"></li>',
                    '<li class="dropdown-header">Сломанные аккаунты</li>',
                    ['label' => 'SMS', 'url' => '/account-list/sms'],
                    ['label' => 'Email', 'url' => '/account-list/email'],
                    ['label' => 'Biography', 'url' => '/account-list/biography'],
                    '<li class="divider"></li>',
                    '<li class="dropdown-header">Заполненные аккаунты</li>',
                    ['label' => 'Filled', 'url' => '/account-list/filled'],
                    '<li class="divider"></li>',
                    '<li class="dropdown-header">Дополнительные стандарты</li>',
                    ['label' => 'Zaliv', 'url' => '/account-list/zaliv'],
                    '<li class="divider"></li>',
                    '<li class="dropdown-header">Список проокси</li>',
                    ['label' => 'Прокси забаненных аккаунтов', 'url' => '/account-list/banned'],
                    ['label' => 'Нерабочие прокси', 'url' => '/account-list/unknown'],
                ],
            ],
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">Булат Гарипов</p>

        <p class="pull-right"></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
