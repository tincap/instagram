<?php

/* @var $this yii\web\View */
/* @var $model \app\models\forms\AddForms */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

$this->title = 'Добавить новый список';

?>

<div class="add-new-list">

    <h3>Добавить список аккаунтов</h3>

    <?php
    $form = ActiveForm::begin([
    'id' => 'add-new-list-form',
    'options' => [],
    ]) ?>

    <?= $form->field($model, 'accountList')->textarea(['rows' => 25])->label(false) ?>

    <?= Html::submitButton('Добавить', ['class' => 'btn btn-primary btn-lg', 'name' => 'add', 'value' => 1]) ?>

    <div style="float:right">
        <?= Html::submitButton('Удалить и добавить', ['class' => 'btn btn-warning btn-lg', 'name' => 'add', 'value' => 2]) ?>
    </div>

    <?php ActiveForm::end() ?>

</div>