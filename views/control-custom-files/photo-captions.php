<?php

/* @var $this yii\web\View */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

/* @var $items array */
/* @var $model \app\models\forms\ControlCustomFiles\PhotoCaptionsForm */

$this->title = 'photoCaptions.txt';

$this->registerCssFile('@web/css/static/control-custom-files/custom.css');

?>

<h3 class="title">photoCaptions.txt</h3>

<div class="info">
    <h5> Всего элементов: <?= count($items) ?> </h5>
</div>

<div>

    <?php
    $form = ActiveForm::begin() ?>

    <?= $form->field($model, 'photoCaptions')->textarea(['rows' => 25])->label(false) ?>

    <?= Html::submitButton('Передобавить', ['class' => 'btn btn-warning', 'name' => 'add']) ?>
    <?php ActiveForm::end() ?>

</div>

<h4 class="title lol">Последние 100 элементов</h4>

<?php

$n = 1;

foreach ($items as $item) {
    echo "<p><strong>" . $n++ . ". " . "</strong>$item</p>";

    if ($n > 100) {
        break;
    }

    echo "<hr>";
}

?>