<?php

/* @var $this yii\web\View */

$this->title = 'Список файлов для заполнения';

?>

<h3 class="title">Список файлов для заполнения</h3>

<p>
    <a href="/control-custom-files/biography"><span class="text-success">biographyList.txt</span> | Для заполнения описания профиля</a>
</p>
<p>
    <a href="/control-custom-files/firstname"><span class="text-success">firstnameList.txt</span> | Имена профилей</a>
</p>
<p>
    <a href="/control-custom-files/photo-captions"><span class="text-success">photoCaptions.txt</span> | Текста для заполнения фотографий</a>
</p>