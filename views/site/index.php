<?php

/* @var $accountList array */
/* @var $this yii\web\View */
/** @var \app\models\Instagram\Account $account */

$this->title = 'Список аккаунтов';
?>
<div class="site-index">

    <table class="table table-bordered table-striped table-hover">

        <?php

        $smsCount = 0;
        $followCount = 0;

        foreach ($accountList as $account) {
            if ($account->error_type == \app\models\Instagram\checkpoint\Discover::ERROR_SMS || $account->error_type == \app\models\Instagram\checkpoint\Discover::ERROR_SMS_ADD_PHONE || $account->error_type == \app\models\Instagram\checkpoint\Discover::ERROR_SMS_GO_BACK) {
                $smsCount++;
            }

            $followCount += $account->follow_count;
        }
        ?>

        <thead class="thead-inverse">
        <tr>
            <th><?= count($accountList) ?></th>
            <th>Список аккаунтов</th>
            <th>Заполнение</th>
            <th>Информация</th>
            <th>Кол-во</th>
            <th class="text-danger" width="100px"><?= $smsCount ?></th>
        </tr>
        </thead>
        <tbody>

        <?php
        foreach ($accountList as $account) :
            ?>

            <tr>
                <td width="40px" class="account_id"><?= $account->id ?></td>
                <td><a class="username" target="_blank" href="https://instagram.com/<?= $account->username ?>"><?= $account->username . ':' . $account->password ?></a></td>
                <td>
                    <?= $account->cleared ? ' <span style="color:#2aba32" title="Очищистка">C</span>' : ' <span style="color:#ba0001" title="Очищистка">C</span>' ?>

                    <?= $account->avatar ? ' <span style="color:#2aba32" title="Аватарка">A</span>' : ' <span style="color:#ba0001" title="Аватарка">A</span>' ?>

                    <?= $account->photo ? ' <span style="color:#2aba32" title="Заполнение">P</span>' : ' <span style="color:#ba0001" title="Заполнение">P</span>' ?>

                    <?= $account->filled ? ' <span style="color:#2aba32" title="Заполнение">F</span>' : ' <span style="color:#ba0001" title="Заполнение">F</span>' ?>

                    <?= $account->biographed ? ' <span style="color:#2aba32" title="Описание">B</span>' : ' <span style="color:#ba0001" title="Описание">B</span>' ?>
                </td>
                <td>
                    <?= $account->zaliv_name != null ? '<a class="username" target="_blank" href="https://instagram.com/' . str_replace('@', '', $account->zaliv_name) . '">' . $account->zaliv_name . '</a> | ' : '' ?>
                    <span class="text-success"><?= $account->follower_count !== null ? $account->follower_count . ' | ' : '' ?></span>
                    <span class="text-danger"><?= $account->following_count !== null ? $account->following_count . ' | ' : '' ?></span>
                    <strong><?= $account->media_count ?></strong>
                </td>
                <td><?= $account->follow_count ?></td>
                <td width="40px" class="<?= $account->working ? 'success' : ($account->banned ? 'danger' : 'warning') ?>"><?= $account->working ? '' : $account->translateErrorType() ?></td>
            </tr>

            <?php
        endforeach;
        ?>

        </tbody>
    </table>

</div>