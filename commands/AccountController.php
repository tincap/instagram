<?php

namespace app\commands;

use app\models\Instagram\Account;
use app\models\Instagram\checkpoint\Discover;
use InstagramAPI\Exception\InstagramException;
use yii\console\Controller;

class AccountController extends Controller
{
    public function actionLogin($left = null, $right = null)
    {
        if (is_null($left)) {
            $left = 1;
            $right = Account::find()->count();
        } else {
            $right = is_null($right) ? $left : $right;
        }

        for ($id = $left; $id <= $right; $id++) {

            $account = Account::findIdentity($id);
            $account->prepare(false);

            try {
                $account->login(true);
            } catch (InstagramException $e) {

                $discover = new Discover($account);
                $error = $discover->identityCheckpointTypeByResponse($e->getResponse());

                echo $error . "\n";

            }
        }
    }
}