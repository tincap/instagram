<?php

namespace app\commands;

use app\models\helpers\ConsoleHelpers;
use app\models\Instagram\Account;
use app\models\Instagram\checkpoint\CheckpointCaptcha;
use app\models\Instagram\checkpoint\CheckpointIncorrectPassword;
use app\models\Instagram\checkpoint\CheckpointMail;
use app\models\Instagram\checkpoint\Discover;
use app\models\Instagram\checkpoint\Exceptions\CheckpointException;
use app\models\Instagram\checkpoint\models\CheckpointSmsModel;
use InstagramAPI\Exception\InstagramException;
use yii\base\Exception;
use yii\console\Controller;

class CheckpointController extends Controller
{
    public function actionFix($left = null, $right = null)
    {
        if (is_null($left)) {
            $left = 1;
            $right = Account::find()->count();
        } else {
            $right = is_null($right) ? $left : $right;
        }

        for ($id = $left; $id <= $right; $id++) {

            $account = Account::findIdentity($id);
            $account->deleteCookieFile();

            $account->prepare(false);

            if ($account->working || $account->banned) {
                continue;
            }

            if ($account->error_type == Discover::ERROR_SMS_ADD_PHONE || $account->error_type == Discover::ERROR_SMS || $account->error_type == Discover::ERROR_SMS_GO_BACK || $account->error_type == Discover::ERROR_INCORRECT_USERNAME) {
                continue;
            }

            echo "\n\n";
            echo $account->id . ". \n";
            echo "-------------------\n";

            try {
                $account->login(true);

                ConsoleHelpers::log("Аккаунт смог авторизоваться", 32);

                $account->error_type = null;
                $account->working = 1;

                $account->save();

            } catch (InstagramException $e) {

                try {

                    $account->prepareCurl();

                    $discover = new Discover($account);
                    $checkpointType = $discover->identityCheckpointTypeByResponse($e->getResponse());

                    switch ($checkpointType) {
                        case Discover::ERROR_EMAIL:

                            ConsoleHelpers::log("Аккаунт требует подтверждения по Email", 33);
                            $checkpoint = new CheckpointMail($account, $discover->getHTML(), $discover->getCheckpointUrl(), true);

                            if ($checkpoint->doCheckpoint()) {
                                ConsoleHelpers::log("Аккаунт подтвержден", 32);

                                $account->error_type = null;
                                $account->working = 1;
                                $account->save();

                            } else {
                                ConsoleHelpers::log("Аккаунт не подтвержден", 31);
                            }

                            break;

                        case Discover::ERROR_CAPTCHA:

                            ConsoleHelpers::log("Капча", 33);

                            echo $discover->getCheckpointUrl() . "\n";

                            $checkpoint = new CheckpointCaptcha($account, $discover->getHTML(), $discover->getCheckpointUrl(), true);
                            if ($checkpoint->doCheckpoint()) {
                                ConsoleHelpers::log("Смогли починить аккаунт", 32);

                                $account->error_type = null;
                                $account->working = 1;
                                $account->save();

                            } else {
                                ConsoleHelpers::log("Не смогли починить аккаунт", 31);
                            }

                            break;

                        case Discover::ERROR_INCORRECT_PASSWORD:

                            ConsoleHelpers::log("Неверный пароль", 33);

                            $checkpoint = new CheckpointIncorrectPassword($account, true);

                            if ($checkpoint->doCheckpoint()) {

                                $account->error_type = null;
                                $account->working = 1;
                                $account->save();

                                ConsoleHelpers::log("Смогли починить аккаунт", 32);
                            } else {
                                ConsoleHelpers::log("Не смогли починить аккаунт", 31);
                            }

                            break;

                        default:
                            throw new CheckpointException("Не смогли определить checkpoint по ошибке: " . $e->getMessage());
                    }
                } catch (Exception $e) {
                    ConsoleHelpers::log($e->getMessage(), 31);
                }
            }
        }
    }

    public function actionSms($accountId)
    {
        try {
            CheckpointSmsModel::fix(true, $accountId);

            ConsoleHelpers::log("OK", 32);

        } catch (Exception $e) {
            ConsoleHelpers::log($e->getMessage(), 31);
        }
    }
}