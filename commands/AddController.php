<?php

namespace app\commands;

use app\models\helpers\FileHelpers;
use app\models\Instagram\Account;
use app\models\Instagram\Proxy;
use yii\console\Controller;
use app\models\Mail\Account as Mail;

class AddController extends Controller
{
    public function actionIndex()
    {
        $table = \Yii::$app->params['table'];

        \Yii::$app->db->createCommand("TRUNCATE `account-{$table}`")->execute();
        \Yii::$app->db->createCommand("TRUNCATE `mail-account-{$table}`")->execute();
        \Yii::$app->db->createCommand("TRUNCATE `proxy-{$table}`")->execute();

        FileHelpers::deleteDirectory(\Yii::getAlias("@settings/data/Mail/cookies/{$table}"));

        $file = \Yii::getAlias('@settings/data/files/accountList.txt');
        $fp = fopen($file, "r");

        $n = 1;

        while (!feof($fp))
        {
            $str = preg_replace("/\s/", "", fgets($fp, 300));

            $e = explode(':', $str);

            $account = new Account();
            $account->username = $e[0];
            $account->password = $e[1];
            $account->save();

            $proxy = new Proxy();
            $proxy->ip = $e[2] . ':' . $e[3];
            $proxy->password = $e[4] . ':' . $e[5];
            $proxy->save();

            $mail = new Mail();
            $mail->username = $e[6];
            $mail->password = $e[7];
            $mail->save();

            echo $n++ . "\n";
        }


        for ($i = 0; $i < 15; $i ++) {
            \Yii::$app->db->createCommand("UPDATE `mail-account-{$table}` SET proxy_id = id - 200 * $i WHERE id > 200 * $i")->execute();
        }

        \Yii::$app->db->createCommand("UPDATE `account-{$table}` SET proxy_id = id, email_id = id")->execute();

        mkdir(\Yii::getAlias("@settings/data/Mail/cookies/{$table}"), 0777);
    }

    public function actionProxyList()
    {
        $file = \Yii::getAlias('@app/data/files/custom/proxyList.txt');

        $fp = fopen($file, "r");

        $n = 1;

        while (!feof($fp))
        {
            $str = preg_replace("/\s/", "", fgets($fp, 300));

            $e = explode(':', $str);

            $proxy = new \app\models\Mail\Proxy();
            $proxy->password = $e[2] . ':' . $e[3];
            $proxy->ip = $e[0] . ':' . $e[1];
            $proxy->save();

            echo $n++ . "\n";
        }

        fclose($fp);
    }
}