<?php

namespace app\commands;

use app\components\ImageResize;
use yii\console\Controller;

class PhotoController extends Controller
{
    public function actionSmall()
    {
        for ($i = 1; $i <= 229; $i++) {
            $photoPath = \Yii::getAlias("@app/data/Instagram/photos/$i.jpg");

            $resize = new ImageResize($photoPath);
            $resize->resizeToHeight(640);
            $resize->save($photoPath);
            $resize = null;

            echo $i . "\n";
        }
    }
}