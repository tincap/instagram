<?php

namespace app\commands;

use app\models\helpers\ConsoleHelpers;
use app\models\Instagram\Account;
use yii\console\Controller;

class MailController extends Controller
{
    public function actionLogin($left = null, $right = null)
    {
        if (is_null($left)) {
            $left = 1;
            $right = Account::find()->count();
        } else {
            $right = is_null($right) ? $left : $right;
        }

        for ($id = $left; $id <= $right; $id++) {

            $account = Account::findIdentity($id);
            $mail = $account->mail;

            if (true === $result = $mail->login()) {
                ConsoleHelpers::log($account->id . ". Mail успешно авторизовался", 32);
            } else {
                ConsoleHelpers::log($account->id . ". Ошибка при авторизации Mail", 31);
            }
        }
    }
}