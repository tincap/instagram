<?php

namespace app\commands;

use app\components\InstagramAPI\Exceptions\InstagramException;
use app\components\InstagramAPI\Instagram;
use app\models\Instagram\Account;
use yii\console\Controller;

class TestController extends Controller
{
    public function actionIndex()
    {
        $account = Account::findIdentity(1);

        $account->prepare(false);
        $account->login();

        $r = $account->instagram->people->getInfoById('1901127826');

        echo $r->getUser()->getUsername() . "\n";

        if ($r->getUser()->getIsPrivate()) {
            echo "private\n";
        } else {
            echo "not private\n";
        }

    }
}