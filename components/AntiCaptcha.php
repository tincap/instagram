<?php

namespace app\components;
use Curl\Curl;
use Yii;

/**
 * Class AntiCaptcha
 * @package settings\components\anticaptcha
 */
class AntiCaptcha
{
    /** @var string Имя файла */
    private $_filename;

    /** @var integer Id капчи  */
    private $_capcha_id;

    private $username;

    const RESPONSE = 'http://anti-captcha.com/res.php';

    public function __construct($imageLink = null, $username)
    {
        $this->username = $username;

        $newFile = Yii::getAlias("@app/data/files/runtime/captcha/{$username}.jpg");

        copy($imageLink, $newFile);

        $this->_filename = $newFile;
    }

    public function deleteCaptchaFile()
    {
        $filePath = Yii::getAlias("@app/data/files/runtime/captcha/{$this->username}.jpg");

        if (file_exists(Yii::getAlias("@app/data/files/runtime/captcha/{$filePath}.jpg"))) {
            unlink($filePath);
        }
    }

    /**
     * Расшифровка капчи
     *
     * @param $body
     * @param int $is_phrase
     * @param int $is_regsense
     * @param int $is_numeric
     * @param int $min_len
     * @param int $max_len
     * @param int $is_russian
     * @return false|string
     */
    public function getTranscript($body = null, $is_phrase = 0, $is_regsense = 0, $is_numeric = 0, $min_len = 0, $max_len = 0, $is_russian = 0)
    {
        $this->getCaptchaId($body, $is_phrase, $is_regsense, $is_numeric, $min_len, $max_len, $is_russian);

        if ($transcript = $this->unravel()) {
            return $transcript;
        } else {
            return false;
        }
    }

    /**
     * Возвращает Id капчи
     *
     * @param null $body
     * @param int $is_phrase
     * @param int $is_regsense
     * @param int $is_numeric
     * @param int $min_len
     * @param int $max_len
     * @param int $is_russian
     * @return bool
     */
    function getCaptchaId($body, $is_phrase, $is_regsense, $is_numeric, $min_len, $max_len, $is_russian)
    {
        $sendhost = "antigate.com";
        $ext = null;

        if ($this->_filename != null) {
            if (!file_exists($this->_filename)) {
                return false;
            }

            $fp = fopen($this->_filename, 'r');

            if ($fp != false) {
                $body = '';
                while (!feof($fp)) {
                    $body .= fgets($fp, 1024);
                }

                fclose($fp);
                $ext = substr($this->_filename, strpos($this->_filename, ".") + 1);
            } else {
                return false;
            }
        }

        $postdata = [
            'method' => 'base64',
            'key' => Yii::$app->params['anti_captcha']['api_key'],
            'body' => base64_encode($body),
            'ext' => $ext,
            'phrase' => $is_phrase,
            'regsense' => $is_regsense,
            'numeric' => $is_numeric,
            'min_len' => $min_len,
            'max_len' => $max_len,
            'is_russian' => $is_russian,
        ];

        $poststr = '';
        while (list($name, $value) = each($postdata)) {
            if (strlen($poststr) > 0) {
                $poststr .= '&';
            }

            $poststr .= $name . '=' . urlencode($value);
        }

        $fp = fsockopen($sendhost, 80);

        if ($fp != false) {
            $header = "POST /in.php HTTP/1.0\r\n";
            $header .= "Host: $sendhost\r\n";
            $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
            $header .= "Content-Length: " . strlen($poststr) . "\r\n";
            $header .= "\r\n$poststr\r\n";
            fputs($fp, $header);

            $resp = '';
            while (!feof($fp)) {
                $resp .= fgets($fp, 1024);
            }

            fclose($fp);

            $result = substr($resp, strpos($resp, "\r\n\r\n") + 4);

        } else {
            return false;
        }

        if (strpos($result, "ERROR") !== false) {
            return false;
        } else {

            $ex = explode("|", $result);
            $captcha_id = $ex[1];

            $this->_capcha_id = $captcha_id;
        }

        return false;
    }

    /**
     * Возвращает разгадку капчи
     *
     * @return false|string
     */
    public function unravel()
    {
        $params = [
            'key' => Yii::$app->params['anti_captcha']['api_key'],
            'action' => 'get',
            'id' => $this->_capcha_id,
        ];

        $curl = new Curl();

        $response = $curl->get(self::RESPONSE, $params);

        if (strpos('ERROR', $response) === false) {

            // Если 4 раза будет ответ "CAPCHA_NOT_READY", то перестаем ждать
            for ($i = 1; $i < 5; $i++) {

                if ($response == 'CAPCHA_NOT_READY') {
                    sleep(5);
                    $response = $curl->get(self::RESPONSE, $params);;
                } elseif (preg_match('/OK\|(.*)/', $response, $found)) {
                    return $found[1];
                }
            }
        }

        return false;
    }
}