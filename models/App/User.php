<?php

namespace app\models\App;

use app\models\helpers\FileDbHelpers;
use yii\base\Exception;

class User
{
    /**
     * Поменять пользователя в базе
     *
     * @param $user
     * @throws Exception
     */
    public static function chooseUser($user)
    {
        if (!self::inDb($user)) {
            throw new Exception("В базе отсутсвует такой пользователь");
        }

        $fp = fopen(self::getUserFilename(), "w+");
        fwrite($fp, $user);
        fclose($fp);
    }

    /**
     * Проверяет, есть ли такой пользователь в базе
     *
     * @param $user
     * @return bool
     */
    public static function inDb($user)
    {
        if (FileDbHelpers::in($user, self::getUserlistFilename())) {
            return true;
        }

        return false;
    }

    /**
     * Добавить нового пользователя в базу
     *
     * @param $newUser
     * @throws Exception
     */
    public static function addNewUser($newUser)
    {
        if (self::inDb($newUser)) {
            throw new Exception("В userlist уже есть такой user");
        }

        FileDbHelpers::add($newUser, self::getUserlistFilename());

        \Yii::$app->db->createCommand("SET SQL_MODE = \"NO_AUTO_VALUE_ON_ZERO\";
SET time_zone = \"+00:00\";

CREATE TABLE `account-{$newUser}` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(40) NOT NULL,
  `password` varchar(40) NOT NULL,
  `proxy_id` int(11) DEFAULT NULL,
  `email_id` int(10) UNSIGNED DEFAULT NULL,
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `cleared` tinyint(1) NOT NULL DEFAULT '0',
  `biographed` tinyint(1) NOT NULL DEFAULT '0',
  `photo` tinyint(1) NOT NULL DEFAULT '0',
  `public` tinyint(1) NOT NULL DEFAULT '0',
  `avatar` tinyint(1) NOT NULL DEFAULT '0',
  `working` tinyint(1) NOT NULL DEFAULT '1',
  `filled` tinyint(1) NOT NULL DEFAULT '0',
  `error_type` varchar(20) DEFAULT NULL,
  `follow_count` int(11) NOT NULL DEFAULT '0',
  `follower_count` int(11) DEFAULT NULL,
  `following_count` int(11) DEFAULT NULL,
  `media_count` int(11) DEFAULT NULL,
  `zaliv_name` varchar(20) DEFAULT NULL,
  `author` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `account-{$newUser}`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `email` (`email_id`);


ALTER TABLE `account-{$newUser}`
MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;")->execute();


        \Yii::$app->db->createCommand("SET SQL_MODE = \"NO_AUTO_VALUE_ON_ZERO\";
SET time_zone = \"+00:00\";

CREATE TABLE `mail-account-{$newUser}` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(40) NOT NULL,
  `password` varchar(40) NOT NULL,
  `proxy_id` int(11) NOT NULL,
  `user_agent` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `mail-account-{$newUser}`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);


ALTER TABLE `mail-account-{$newUser}`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;")->execute();

        \Yii::$app->db->createCommand("SET SQL_MODE = \"NO_AUTO_VALUE_ON_ZERO\";
SET time_zone = \"+00:00\";

CREATE TABLE `proxy-{$newUser}` (
  `id` int(11) NOT NULL,
  `ip` varchar(25) NOT NULL,
  `password` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `proxy-{$newUser}`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `proxy-{$newUser}`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;")->execute();

        \Yii::$app->db->createCommand("SET SQL_MODE = \"NO_AUTO_VALUE_ON_ZERO\";
SET time_zone = \"+00:00\";

CREATE TABLE `mobile-proxy-{$newUser}` (
  `id` int(11) NOT NULL,
  `ip` varchar(25) NOT NULL,
  `password` varchar(40) NOT NULL,
  `update_url` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `mobile-proxy-{$newUser}`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `update_url` (`update_url`);


ALTER TABLE `mobile-proxy-{$newUser}`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;")->execute();


        mkdir(\Yii::getAlias("@app/data/Mail/cookies/{$newUser}"), 0777);

    }

    public static function getActiveUser()
    {
        $firstNameListFile = fopen(self::getUserFilename(), 'r');
        $out = preg_replace("/\s/", "", fgets($firstNameListFile, 20));
        fclose($firstNameListFile);

        return $out;
    }

    public static function getUserlistFilename()
    {
        return \Yii::getAlias("@app/data/app/user/userlist.txt");
    }

    public static function getUserFilename()
    {
        return \Yii::getAlias("@app/data/app/user/user.txt");
    }
}