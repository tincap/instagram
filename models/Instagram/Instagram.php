<?php

namespace app\models\Instagram;

class Instagram extends \InstagramAPI\Instagram
{
    public static $allowDangerousWebUsageAtMyOwnRisk = true;

    public function setUser($username, $password) {
        parent::_setUser($username, $password);
    }
}