<?php

namespace app\models\Instagram\SmsServices;

use Curl\Curl;

class SmsActivate
{
    CONST HOST = 'http://sms-activate.ru/stubs/handler_api.php';

    CONST SERVICE = 'ig';
    CONST FORWARD = 0;

    private $apiKey = null;

    /**
     * 0 - Россия
     * 1 - Украина
     * 2 - Казахстан
     *
     * @var int
     */
    private $country = null;

    public function __construct($apiKey = null, $country)
    {
        $this->apiKey = $apiKey;
        $this->country = $country;
    }

    public function request($action, $params = [])
    {
        $curl = new Curl();
        $params['api_key'] = $this->apiKey;
        $params['action'] = $action;
        $params['service'] = self::SERVICE;
        $params['forward'] = self::FORWARD;

        if ($this->country != null) {
            $params['country'] = $this->country;
        }

        return $curl->get(self::HOST, $params);
    }

    public static function getApiKey($author)
    {
        return \Yii::$app->params['sms_activate_keys'][$author];
    }

    /**
     * Количество телефонов Instagram
     *
     * @return bool
     */
    public function getNumberCount()
    {
        $action = 'getNumbersStatus';

        $result = $this->request($action);

        if (is_string($result)) {
            $result = json_decode($result);
        }

        $service = self::SERVICE . '_' . self::FORWARD;

        if (isset($result->$service)) {
            return $result->$service;
        }

        return false;
    }

    /**
     * Баланс
     *
     * @return bool
     */
    public function getBalance()
    {
        $action = 'getBalance';

        $result = self::request($action);

        if (is_string($result) && preg_match('/ACCESS_BALANCE:(\d+)/iu', $result, $found)) {

            if (isset($found[1])) {
                return $found[1];
            }

        }

        return false;
    }

    /**
     * Получить номер
     *
     * @return array|bool
     */
    public function getNumber()
    {
        $action = 'getNumber';

        $result = self::request($action);

        if (is_string($result)) {
            if (preg_match('/ACCESS_NUMBER:(\d+):(\d+)/', $result, $found)) {

                return [
                    'id' => $found[1],
                    'number' => $found[2],
                ];
            }
        }

        return false;
    }

    /**
     *
     *
     * @param $id
     * @param $status
     *      -1 - отменить активацию
     *      1 - сообщить о готовности номера (смс на номер отправлено)
     *      3 - запросить еще один код (бесплатно)
     *      6 - завершить активацию(если был статус "код получен" - помечает успешно и завершает, если был "подготовка" - удаляет и помечает ошибка, если был статус "ожидает повтора" - переводит активацию в ожидание смс)
     *      8 - сообщить о том, что номер использован и отменить активацию
     * @return string
     */
    public function setStatus($id, $status)
    {
        $action = 'setStatus';

        $params = [
            'id' => $id,
            'status' => $status,
        ];

        $result = $this->request($action, $params);

        return $result;
    }

    /**
     * Получение статуса сообщения
     *
     * @param $id
     * @return bool
     */
    public function getStatus($id)
    {
        $action = 'getStatus';

        $params = [
            'id' => $id,
        ];

        $result = $this->request($action, $params);

        if (is_string($result)) {
            if (preg_match('/STATUS_OK:(.*)/iu', $result, $found)) {
                return isset($found[1]) ? $found[1] : false;
            }
        }

        return $result;
    }
}