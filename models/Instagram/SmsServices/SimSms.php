<?php


namespace app\models\Instagram\SmsServices;

use Curl\Curl;

class SimSms
{
    CONST HOST = 'http://simsms.org/priemnik.php';
    CONST SERVICE = 'opt16';

    private $apiKey = null;

    private $country;

    /**
     * SimSms constructor.
     * @param String $apiKey
     */
    public function __construct($apiKey = null)
    {
        $this->apiKey = $apiKey;
    }

    /**
     * Запросы
     *
     * @param $method
     * @param array $params
     * @return bool|mixed|string
     */
    public function request($method, $params = [])
    {
        $curl = new Curl();

        $params['apikey'] = $this->apiKey;
        $params['metod'] = $method;
        $params['service'] = self::SERVICE;

        $params['country'] = $this->country;

        $response = $curl->get(self::HOST, $params);

        $response = json_decode($response);

        if (isset($response->response)) {
            return $response;
        }

        return false;
    }

    /**
     * Баланс
     *
     * @return bool
     */
    public function getBalance()
    {
        $method = 'get_balance';

        $result = $this->request($method);

        if (isset($result->balance)) {
            return $result->balance;
        }

        return false;
    }

    /**
     * Получить кол-во номеров
     *
     * @return bool
     */
    public function getNumberCount()
    {
        $method = 'get_count';

        $result =  $this->request($method, [
            'service_id' => 'instagram',
        ]);

        if (isset($result->{'counts Instagram'})) {
            return $result->{'counts Instagram'};
        }

        return false;
    }

    /**
     * Получить номер
     *
     * @return array|bool
     */
    public function getNumber()
    {
        $method = 'get_number';

        $result = $this->request($method, [
            'country' => 'ru',
            'id' => 1,
        ]);


        if (empty($result->id) || empty($result->number)) {
            return false;
        }

        return [
            'id' => $result->id,
            'number' => '7' . $result->number,
        ];
    }

    /**
     * Получить sms
     *
     * @param $id
     * @return bool
     */
    public function getSms($id)
    {
        $method = 'get_sms';

        $result = $this->request($method, [
            'country' => 'ru',
            'id' => $id,
        ]);

        if (!empty($result->text)) {
            return $result->text;
        }

        return false;
    }

    /**
     * Отмена
     *
     * @param $id
     * @return bool|mixed|string
     */
    public function cancel($id)
    {
        $method = 'denial';

        return $this->request($method, [
            'country' => 'ru',
            'id' => $id,
        ]);
    }
}