<?php


namespace app\models\Instagram\SmsServices;


use app\models\helpers\ConsoleHelpers;
use app\models\Instagram\SmsServices\Exceptions\SmsServiceException;

class BasicSmsServiceModel
{
    /** @var string */
    private $smsService;

    /** @var SmsActivate */
    private $smsActivate;

    /** @var OnlineSim */
    private $onlineSim;

    /** @var SimSms */
    private $simSms;

    /** @var array */
    public $phone;

    /**
     * Какое по счету это обращение к сервису
     *
     * @var int
     */
    private $smsNumber;

    /** @var bool */
    private $debug = false;

    CONST SMS_SERVICE_SMS_ACTIVATE = 'sms_activate';
    CONST SMS_SERVICE_ONLINE_SIM = 'online_sim';
    CONST SMS_SERVICE_SMS_SIM = 'sms_sim';

    /**
     * BasicPhonesServiceModel constructor.
     * @param $debug
     * @param null $phone
     * @param int $smsNumber
     * @param $smsService
     * @param $country
     */
    public function __construct(
        $debug,
        $phone = null,
        $smsNumber = 1,
        $smsService = null,
        $country = 0
    ) {
        $this->debug = $debug;

        $this->smsActivate = new SmsActivate(\Yii::$app->params['sms_services_keys']['sms_activate'][\Yii::$app->params['sms_services_main_author']], $country);
        $this->onlineSim = new OnlineSim(\Yii::$app->params['sms_services_keys']['online_sim'][\Yii::$app->params['sms_services_main_author']]);
        $this->simSms = new SimSms(\Yii::$app->params['sms_services_keys']['sim_sms'][\Yii::$app->params['sms_services_main_author']]);

        $this->smsService = $smsService;
        $this->smsNumber = $smsNumber;
        $this->phone = $phone;
    }

    /**
     * @return bool|string
     * @throws SmsServiceException
     */
    public function getCode()
    {
        switch ($this->smsService) {
            case self::SMS_SERVICE_SMS_ACTIVATE:

                if ($this->debug)
                    ConsoleHelpers::log("Ждем прихода сообщения в sms-activate.ru", 35);

                // Сообщаем sms-activate, что мы готовы получить код
                if ($this->smsNumber > 1) {
                    $this->smsActivate->setStatus($this->phone['id'], 3);
                } else {
                    $this->smsActivate->setStatus($this->phone['id'], 1);
                }

                $n = 0;

                do {
                    sleep(5);
                    $smsText = $this->smsActivate->getStatus($this->phone['id']);
                    $n++;

                    if ($this->debug)
                        echo '.';

                } while (($smsText == 'STATUS_WAIT_CODE' || preg_match('/STATUS_WAIT_RETRY/iu', $smsText)) && $n < 25);

                if ($this->debug)
                    echo "\n";

                if (!is_numeric($smsText)) {

                    if ($this->smsNumber > 1) {
                        $this->success();
                    } else {
                        $this->cancel();
                    }

                    throw new SmsServiceException("Не дождались кода в sms-activate.ru. " . $smsText);
                }

                $code = self::getCodeFromSmsText($smsText);

                if ($this->debug)
                    ConsoleHelpers::log("Код из sms-activate.ru: $code", 32);

                return $code;

            case self::SMS_SERVICE_ONLINE_SIM:

                if ($this->debug)
                    ConsoleHelpers::log("Ждем прихода сообщения в onlinesim.ru", 35);

                if ($this->smsNumber > 1) {
                    $this->onlineSim->isReady($this->phone['id']);
                } else {
                    $this->onlineSim->getNumRepeat($this->phone['number']);
                }

                $n = 0;

                do {
                    sleep(5);
                    $code = $this->onlineSim->getCode($this->phone['id']);
                    $n++;

                    if ($this->debug)
                        echo '.';

                } while ($code == false && $n < 25);

                if ($this->debug)
                    echo "\n";

                if (!is_numeric($code)) {
                    throw new SmsServiceException("Не дождались кода в onlinesim.ru");
                }

                if ($this->debug)
                    ConsoleHelpers::log("Код из onlinesim.ru: $code", 32);

                return $code;

            case self::SMS_SERVICE_SMS_SIM:

                if ($this->debug)
                    ConsoleHelpers::log("Ждем прихода сообщения в simsms.org", 35);

                $n = 0;

                do {
                    sleep(5);
                    $smsText = $this->simSms->getSms($this->phone['id']);
                    $n++;

                    if ($this->debug)
                        echo ".";

                } while ($smsText == false && $n <= 25);

                if ($this->debug)
                    echo "\n";

                $code = self::getCodeFromSmsText($smsText);

                if (!is_numeric($code)) {
                    $this->cancel();
                    throw new SmsServiceException("Не дождались кода в simsms.org");
                }

                if ($this->debug)
                    ConsoleHelpers::log("Код из simsms.org: $code", 32);

                return $code;
        }

        throw new SmsServiceException("Не выбран Sms Service");
    }

    /**
     * Отмена
     */
    public function cancel()
    {
        if ($this->smsService == self::SMS_SERVICE_SMS_ACTIVATE) {
            $this->smsActivate->setStatus($this->phone['id'], -1);
        }

        if ($this->smsService == self::SMS_SERVICE_SMS_SIM) {
            $this->simSms->cancel($this->phone['id']);
        }
    }

    public function success()
    {
        if ($this->smsService == self::SMS_SERVICE_SMS_ACTIVATE) {
            $this->smsActivate->setStatus($this->phone['id'], 6);
        }

        if ($this->smsService == self::SMS_SERVICE_ONLINE_SIM) {
            $this->onlineSim->setOperationOk($this->phone['id']);
        }
    }

    /**
     * Получаем номера
     *
     * @return array|bool|mixed|string
     */
    public function getNumber()
    {
        switch ($this->smsService) {
            case self::SMS_SERVICE_SMS_ACTIVATE:

                if ($this->debug)
                    ConsoleHelpers::log("Получаем номер телефона в sms-activate.ru", 35);

                $this->phone = $this->smsActivate->getNumber();
                return $this->phone;

            case self::SMS_SERVICE_ONLINE_SIM:

                if ($this->debug)
                    ConsoleHelpers::log("Получаем номер телефона в onlinesim.ru", 35);

                $this->phone = $this->onlineSim->getNumber();
                return $this->phone;

            case self::SMS_SERVICE_SMS_SIM:

                if ($this->debug)
                    ConsoleHelpers::log("Получаем номер телефона в simsms.org", 35);

                $this->phone = $this->simSms->getNumber();
                return $this->phone;
            }

            return false;
    }

    /**
     * Выбираем сервис
     * @return bool|string
     * @throws SmsServiceException
     */
    public function chooseService()
    {
        if ($this->smsActivate->getNumberCount() > 0 && $this->smsActivate->getBalance() > 20) {

            if ($this->debug)
                ConsoleHelpers::log("Сервисом для принятия смс выбрали службу " . self::SMS_SERVICE_SMS_ACTIVATE, 34);

            $this->smsService = self::SMS_SERVICE_SMS_ACTIVATE;
            return self::SMS_SERVICE_SMS_ACTIVATE;
        }

        if ($this->onlineSim->getNumberCount() > 0 && $this->onlineSim->getBalance() > 20) {

            if ($this->debug)
                ConsoleHelpers::log("Сервисом для принятия смс выбрали службу " . self::SMS_SERVICE_ONLINE_SIM, 34);

            $this->smsService = self::SMS_SERVICE_ONLINE_SIM;
            return self::SMS_SERVICE_ONLINE_SIM;
        }

        if ($this->simSms->getNumberCount() > 5 && $this->simSms->getBalance() > 20) {

            if ($this->debug)
                ConsoleHelpers::log("Сервисом для принятия смс выбрали службу " . self::SMS_SERVICE_SMS_SIM, 34);

            $this->smsService = self::SMS_SERVICE_SMS_SIM;
            return self::SMS_SERVICE_SMS_SIM;
        }

        throw new SmsServiceException("Ни в одном сервисе нет телефонов");
    }

    /**
     * Получить код из сообщения
     *
     * @param $smsText
     * @return bool
     */
    public static function getCodeFromSmsText($smsText)
    {
        if (preg_match('/(\d+\s*\d+)/iu', $smsText, $found)) {
            return isset($found[1]) ? preg_replace('/\s/', '', $found[1]) : null;
        }

        return null;
    }

    /**
     * @return null|string
     */
    public function getSmsService()
    {
        return $this->smsService;
    }

    /**
     * @param $smsService
     */
    public function setSmsService($smsService)
    {
        $this->smsService = $smsService;
    }
}