<?php

namespace app\models\Instagram\SmsServices;

use app\models\Instagram\SmsServices\Exceptions\SmsServiceException;
use Curl\Curl;

class OnlineSim
{
    CONST HOST = 'http://onlinesim.ru/api/';

    private $apiKey = null;

    /**
     * OnlineSim constructor.
     * @param null $apiKey
     */
    public function __construct($apiKey = null)
    {
        $this->apiKey = $apiKey;
    }

    /**
     * Выполнение запросов
     *
     * @param $method
     * @param array $params
     * @return string
     */
    public function request($method, $params = [])
    {
        $curl = new Curl();

        $url = self::HOST . $method . '.php';

        $params['apikey'] = $this->apiKey;

        return $curl->get($url, $params);
    }

    /**
     * @return string
     */
    public function getServiceList()
    {
        return $this->request('getServiceList');
    }

    /**
     * Получить кол-во номеров
     * @return bool
     * @throws SmsServiceException
     */
    public function getNumberCount()
    {
        $result = $this->getServiceList();

        if (isset($result->response)) {
            throw new SmsServiceException("OnlineSms Error: " . $result->response);
        }

        if (isset($result->{24}) && isset($result->{24}->limit)) {
            return $result->{24}->limit;
        }

        return false;
    }

    /**
     * Получить номер
     *
     * @return array|bool
     */
    public function getNumber()
    {
        $result = $this->request('getNum', [
            'service' => 'instagram',
        ]);

        if (isset($result->response) && isset($result->tzid) && $result->response == 1) {

            if (false === $phoneNumber = $this->getNumberOfState($result->tzid)) {
                return false;
            }

            $phoneNumber = preg_replace('/\+/', '', $phoneNumber);

            return [
                'id' => $result->tzid,
                'number' => $phoneNumber,
            ];
        }

        return false;
    }

    public function getNumberOfState($tzid)
    {
        $result = $this->request('getState', [
            'tzid' => $tzid,
        ]);

        if (isset($result[0]) && isset($result[0]->number)) {
            return $result[0]->number;
        }

        return false;
    }

    /**
     * Запрос на повторное использование виртуального номера
     *
     * @param $number
     * @return bool
     */
    public function getNumRepeat($number)
    {
        $result = $this->request('getNumRepeat', [
            'service' => 'instagram',
            'number' => $number,
        ]);

        if (isset($result->response) && isset($result->tzid) && $result->response == 1) {
            return true;
        }

        return false;
    }

    /**
     * Баланс
     *
     * @return bool
     */
    public function getBalance()
    {
        $result = $this->request('getBalance');

        if (isset($result->balance)) {
            return $result->balance;
        }

        return false;
    }

    /**
     * Готов получить Смс
     *
     * @param $tzid
     * @return bool
     */
    public function isReady($tzid)
    {
        $result = $this->request('setForwardStatusEnable', [
            'tzid' => $tzid,
        ]);

        if (isset($result->response) && $result->response == 1) {
            return true;
        }

        return false;
    }

    /**
     * Получить код из сообщения
     *
     * @param $tzid
     * @return string
     */
    public function getCode($tzid)
    {
        $result = $this->request('getState', [
            'tzid' => $tzid,
        ]);

        if (isset($result[0]->msg)) {
            return $result[0]->msg;
        }

        return false;
    }

    /**
     * Сообщает сервису, что все прошло успешно
     *
     * @param $tzid
     * @return bool
     */
    public function setOperationOk($tzid)
    {
        $result = $this->request('getState', [
            'tzid' => $tzid,
        ]);

        if (isset($result->response) && $result->response == 1) {
            return true;
        }

        return false;
    }
}