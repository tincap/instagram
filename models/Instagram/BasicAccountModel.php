<?php

namespace app\models\Instagram;

use app\models\App\User;
use app\models\helpers\InstagramSignatureUtils;
use app\models\Instagram\Exceptions\AccountModelException;
use Curl\Curl;
use InstagramAPI\Constants;
use yii\db\ActiveRecord;
use app\models\Mail\Account as Mail;

/**
 * This is the model class for table "account".
 *
 * @property integer $id
 * @property string  $username
 * @property string  $password
 * @property integer $proxy_id
 * @property integer $mobile_proxy_id
 * @property integer $email_id
 * @property integer $banned
 * @property integer $working
 * @property integer $cleared
 * @property integer $biographed
 * @property integer $filled
 * @property string  $error_type
 * @property integer $public
 * @property integer $avatar
 * @property integer $photo
 * @property integer $follow_count - сколько подписок сделал софт
 * @property integer $follower_count - кол-во подписчиков
 * @property integer $following_count - кол-во подписок
 * @property integer $media_count
 * @property string  $zaliv_name
 * @property string  $author
 *
 * @property Proxy proxy
 * @property MobileProxy mobileProxy
 * @property Mail mail
 */
class BasicAccountModel extends ActiveRecord
{
    /** @var Curl */
    public $curl;

    /** @var Instagram */
    public $instagram;

    /** @var boolean */
    private $mobileProxyId;

    private function getCookieFile()
    {
        return \Yii::getAlias("@vendor/mgp25/instagram-php/sessions/{$this->username}/{$this->username}-cookies.dat");
    }

    private function getSettingsFile()
    {
        return \Yii::getAlias("@vendor/mgp25/instagram-php/sessions/{$this->username}/{$this->username}-settings.dat");
    }

    /**
     * Авторизация
     *
     * @param bool   $forceLogin         Force login to Instagram instead of resuming previous session.
     * @return \InstagramAPI\Response\LoginResponse|null
     */
    public function login($forceLogin = false)
    {
        if ($this->isLoggedIn() && !$forceLogin) {
            return null;
        }

        return $this->instagram->login($this->username, $this->password);
    }

    /**
     * Подготовка аккаунта
     * @param $mobileProxyId
     */
    public function prepare($mobileProxyId)
    {
        Instagram::$allowDangerousWebUsageAtMyOwnRisk = true;
        $this->instagram = new Instagram();

        $this->instagram->setUser($this->username, $this->password);
        $this->instagram->client->saveCookieJar();

        if ($mobileProxyId) {
            $this->instagram->setProxy($this->getMobileProxyInGuzzleFormat($mobileProxyId));
        } else {
            $this->instagram->setProxy($this->getProxyInGuzzleFormat());
        }

        $this->mobileProxyId = $mobileProxyId;
    }

    public function prepareCurl()
    {
        $this->curl = new Curl();

        if ($this->mobileProxyId === null) {
            throw new AccountModelException("Не определена переменная mobileProxyFlag. Перед работой с curl аккаунта нужно запустить prepare()");
        }

        if ($this->mobileProxyId) {
            $proxy = MobileProxy::findIdentity($this->mobileProxyId);
        } else {
            $proxy = Proxy::findIdentity($this->proxy_id);
        }

        $this->curl->setOpt(CURLOPT_HTTPPROXYTUNNEL, 1);
        $this->curl->setOpt(CURLOPT_PROXY, $proxy->ip);
        $this->curl->setOpt(CURLOPT_PROXYUSERPWD, $proxy->password);

        $this->curl->setHeader('Connection', 'close');
        $this->curl->setHeader('Accept', '*/*');
        $this->curl->setHeader('X_IG_Capabilities', Constants::X_IG_Capabilities);
        $this->curl->setHeader('X-IG-Connection-Type', Constants::X_IG_Connection_Type);
        $this->curl->setHeader('X-IG-Connection-Speed', mt_rand(1000, 3700).'kbps');
        $this->curl->setHeader('X_FB_HTTP_Engine', Constants::X_FB_HTTP_Engine);
        $this->curl->setHeader('Content-Type', Constants::CONTENT_TYPE);
        $this->curl->setHeader('Accept-Language', Constants::ACCEPT_LANGUAGE);

        $userAgent = $this->getUserAgent();

        if ($userAgent === null) {
            $userAgent = InstagramSignatureUtils::generateUserAgent();
        }

        $this->curl->setUserAgent($userAgent);

        $cookieData = $this->getCookieData();

        foreach ($cookieData as $key => $value) {
            $this->curl->setCookie($key, $value);
        }
    }

    /**
     * Проверяет залогининен ли пользователь
     *
     * @return bool
     */
    public function isLoggedIn()
    {
        $data = $this->getCookieData();

        if (
            isset($data['csrftoken']) &&
            isset($data['mid']) &&
            isset($data['sessionid']) &&
            isset($data['ds_user']) &&
            isset($data['ds_user_id'])
        ) {
            return true;
        }

        return false;
    }

    /**
     * Proxy in Guzzle format
     *
     * @param $mobile_proxy_id
     * @return string
     * @throws AccountModelException
     */
    public function getMobileProxyInGuzzleFormat($mobile_proxy_id)
    {
        $proxy = MobileProxy::findIdentity($mobile_proxy_id);

        if ($proxy == null) {
            throw new AccountModelException("Неверный mobile_proxy_id");
        }

        return "http://{$proxy->password}@{$proxy->ip}";
    }

    /**
     * Proxy in Guzzle format
     *
     * @return string
     */
    public function getProxyInGuzzleFormat()
    {
        $proxy = Proxy::findIdentity($this->proxy_id);

        return "http://{$proxy->password}@{$proxy->ip}";
    }

    /**
     * Вернуть кукисы аккаунта
     *
     * @return array
     */
    public function getCookieData()
    {
        $cookieData = [];

        $data = json_decode($this->instagram->client->getCookieJarAsJSON());

        foreach ($data as $item) {
            $cookieData[$item->Name] = $item->Value;
        }

        if (count($cookieData) == 0) {

            $cookieFilePath = \Yii::getAlias("@vendor/mgp25/instagram-php/sessions/{$this->username}/{$this->username}-cookies.dat");

            if (file_exists($cookieFilePath)) {
                $text = file_get_contents($cookieFilePath);

                $data = json_decode($text);

                foreach ($data as $item) {
                    $cookieData[$item->Name] = $item->Value;
                }
            }

            return $cookieData;
        }

        return $cookieData;
    }

    /**
     * Удалить куки файл
     */
    public function deleteCookieFile()
    {
        if (file_exists($this->getCookieFile())) {
            unlink($this->getCookieFile());

        }

        if (file_exists($this->getSettingsFile())) {
            unlink($this->getSettingsFile());
        }
    }

    /**
     * Вернуть настройки аккаунта
     *
     * @return array
     */
    public function getSettingsData()
    {
        $settingsFilePath = \Yii::getAlias("@vendor/mgp25/instagram-php/sessions/{$this->username}/{$this->username}-settings.dat");

        if (file_exists($settingsFilePath)) {

            $text = file_get_contents($settingsFilePath);

            $text = ltrim($text, 'FILESTORAGEv2;');

            return json_decode($text);
        }

        return null;
    }

    public function getUserAgent()
    {
        $settings = $this->getSettingsData();

        if (isset($settings->devicestring)) {
            return "Instagram " . Constants::IG_VERSION . " Android (" . $settings->devicestring . "; en_US)";
        }

        return null;
    }

    /**
     * Proxy
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProxy()
    {
        return static::hasOne(Proxy::className(), ['id' => 'proxy_id']);
    }

    /**
     * Mobile Proxy
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMobileProxy()
    {
        return static::hasOne(Proxy::className(), ['id' => 'mobile_proxy_id']);
    }

    /**
     * Email
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMail()
    {
        return static::hasOne(Mail::className(), ['id' => 'email_id']);
    }

    /**
     * Поиск по username
     *
     * @param $username
     * @return static
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * Поиск по id
     *
     * @param $id
     * @return static
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /** @inheritdoc */
    public static function tableName()
    {
        return 'account-' . User::getActiveUser();
    }

}