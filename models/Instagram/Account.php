<?php

namespace app\models\Instagram;

use app\models\Instagram\checkpoint\Discover;
use app\models\Mail\Exceptions\MailLoginException;
use app\models\Mail\Message;
use InstagramAPI\Exception\InstagramException;

class Account extends BasicAccountModel
{
    public function getMediaCount()
    {
        $selfInfo = $this->instagram->people->getSelfInfo();

        return $selfInfo->getUser()->getMediaCount();
    }

    public function translateErrorType()
    {
        switch ($this->error_type) {
            case Discover::ERROR_SMS:
                return 'смс';
                break;

            case Discover::ERROR_SMS_ADD_PHONE:
                return 'смс';
                break;

            case Discover::ERROR_SMS_GO_BACK:
                return 'смс';
                break;

            case Discover::ERROR_INCORRECT_PASSWORD:
                return 'Неверный пароль';
                break;

            case Discover::ERROR_EMAIL:
                return 'email';
                break;

            case Discover::ERROR_DISABLED:
                return 'Забанен';
                break;

            case Discover::ERROR_INCORRECT_USERNAME:
                return 'Неверный логин';
                break;

            case Discover::ERROR_PASSWORD:
                return 'Слетел пароль';
                break;

            case Discover::ERROR_CHANGED_EMAIL:
                return 'Email угнан';
                break;

            case Discover::ERROR_FEEDBACK:
                return 'feedback';
                break;

            case Discover::ERROR_CAPTCHA:
                return 'Капча';
                break;

            case Discover::ERROR_BLOCKED_EMAIL:
                return 'Нерабочий email';
                break;

            default:
                return $this->error_type == null ? 'unknown' : $this->error_type;
        }
    }

    public function check()
    {
        $this->prepare(false);

        try {
            $this->instagram->isMaybeLoggedIn = true;
            $this->instagram->login($this->username, $this->password);

            return true;
        } catch (InstagramException $e) {

            $discover = new Discover($this);

            $this->error_type = $discover->identityCheckpointTypeByResponse($e->getResponse());

            if ($discover->isBanned()) {
                $this->banned = 1;
            }

            $this->working = 0;
            $this->save();

            if ($this->error_type == null) {
                print_r($e->getResponse());
            }

            return $this->error_type;
        }
    }

    /**
     * Возвращает файл со списком подписок аккаунта
     *
     * @return bool|string
     */
    public function getFollowListFile()
    {
        return \Yii::getAlias('@app/data/Instagram/follow_list/' . $this->username . '.txt');
    }

    /**
     * Проверяет, угнали ли mail аккаунта
     */
    public function checkChangedMail() {
        /** @var \app\models\mail\Account $mail */
        $mail = $this->mail;
        if (!$mail->login()) {
            throw new MailLoginException("Mail аккаунта не может авторизоваться");
        }

        $message = new Message($mail);
        $messagesList = $message->getMessagesList();

        foreach ($messagesList as $item) {
            if (preg_match('/Instagram/iu', $item['from']) && preg_match('/Email Changed on Instagram/iu', $item['subject'])) {
                $this->error_type = Discover::ERROR_CHANGED_EMAIL;
                $this->banned = 1;
                $this->working = 0;
                $this->save();

                return true;
            }
        }

        $message->clearMessages();

        return false;
    }
}