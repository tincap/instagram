<?php

namespace app\models\Instagram\checkpoint;

use app\models\helpers\AccountHelpers;
use app\models\helpers\ConsoleHelpers;
use app\models\helpers\InstagramSignatureUtils;
use app\models\helpers\ParserHelper;
use app\models\Instagram\Account;
use app\models\Instagram\checkpoint\Exceptions\CheckpointException;
use app\models\Mail\Exceptions\MailLoginException;
use app\models\Mail\Message;
use Curl\Curl;
use InstagramAPI\Exception\InstagramException;

/**
 * Instagram сбросил пароль
 * @package app\models\instagram\checkpoint
 */
class CheckpointPassword
{
    /** @var Account $account */
    public $account;

    /** @var bool */
    public $debug;

    public $checkpointUrl;

    public $errorMessage = '';

    public function __construct($account, $debug = false)
    {
        $this->account = $account;
        $this->debug = $debug;
    }

    /**
     * @return bool
     * @throws CheckpointException
     */
    public function doCheckpoint()
    {
        $this->checkpointSecondStep($this->checkpointFirstStep());

        try {
            $this->account->login(true);
            $this->account->error_type = null;
            $this->account->working = 1;
            $this->account->save();

            return true;
        } catch (InstagramException $e) {
            throw new CheckpointException("Не смог авторизоваться после починки");
        }
    }

    /**
     * Ищем сообщение в Mail аккаунта и возвращаем ссылку для восстановления пароля
     * @return bool|string
     * @throws CheckpointException
     * @throws MailLoginException
     */
    public function checkpointFirstStep()
    {
        /** @var \app\models\mail\Account $mail */
        $mail = $this->account->mail;

        if (!$mail->login()) {
            throw new MailLoginException("Mail аккаунта не смог авторизоваться");
        }

        $mail->prepareCurl();
        $message = new Message($mail);

        $n = 1;
        $flag = false;

        do {

            $messagesList = $message->getMessagesList();

            foreach ($messagesList as $item) {
                if (preg_match('/Instagram/iu', $item['from']) && preg_match('/(Reset Your Password)|(Password reset on Instagram)|(Смена пароля на Instagram)|(Скидання пароля в Instagram)/iu', $item['subject'])) {
                    $messageId = $item['id'];
                    $flag = true;

                    break;
                }
            }

            sleep(15);
            $n++;

        } while ($n < 15 && !$flag);

        if (!isset($messageId)) {
            $this->account->error_type = 'incorrect_password';
            $this->account->save();

            throw new CheckpointException("Не нашли сообщение на восстановление пароля");
        }

        $messageText = $message->showMessage($messageId);

        preg_match('/(https:\/\/instagram\.com\/accounts\/password\/reset\/confirm\/\w+\/[\w+-]*\/)/iu', urldecode($messageText), $found);

        if (!isset($found[1])) {
            $this->account->error_type = 'incorrect_password';
            $this->account->save();

            throw new CheckpointException("Не нашли кнопку для восстановления пароля");
        }

        if ($this->debug)
            ConsoleHelpers::log("url: " . $found[1], 37);

        $found[1] = str_replace('https://instagram.com', 'https://www.instagram.com', $found[1]);

        return $found[1];
    }

    public function checkpointSecondStep($url)
    {
        $curl = new Curl();
        $curl->setUserAgent(InstagramSignatureUtils::generateWebUserAgent());

        $curl->setOpt(CURLOPT_FOLLOWLOCATION, true);
        $html = $curl->get($url);

        $curl->setCookie('csrftoken', $curl->getCookie('csrftoken'));
        $curl->setCookie('mid', $curl->getCookie('mid'));

        $newPassword = AccountHelpers::generatePassword();

        $formData = ParserHelper::getFormData($html, 0, [
            'new_password1' => $newPassword,
            'new_password2' => $newPassword,
        ]);

        if ($formData == null) {

            if (preg_match('/This is not a valid link/iu', $html)) {

                /** @var \app\models\mail\Account $mail */
                $mail = $this->account->mail;

                if (!$mail->login()) {
                    throw new MailLoginException("Mail не смог авторизоваться");
                }

                $mail->prepareCurl();

                $message = new Message($mail);
                $message->clearMessages();

                $this->account->error_type = 'incorrect_password';
                $this->account->save();

                throw new CheckpointException("Невалидная ссылка");
            }

            throw new CheckpointException("Не смогли вытащить form");
        }

        $formData['action'] = $url;

        $curl->post($formData['action'], $formData['data']);

        $this->account->password = $newPassword;
        $this->account->save();

        return true;
    }
}