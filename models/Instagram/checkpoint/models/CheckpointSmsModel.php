<?php

namespace app\models\Instagram\checkpoint\models;


use app\models\Instagram\Account;
use app\models\Instagram\checkpoint\CheckpointSms;
use app\models\Instagram\checkpoint\Discover;
use app\models\Instagram\checkpoint\Exceptions\CheckpointException;
use app\models\Instagram\SmsServices\BasicSmsServiceModel;
use InstagramAPI\Exception\InstagramException;

class CheckpointSmsModel
{
    /**
     * Проверячет, требует ли аккаунт подтверждения смс
     *
     * @param bool $debug
     * @param $accountId
     * @param $phoneId
     * @param $phoneNumber
     * @param $smsService
     * @param $smsNumber
     * @throws CheckpointException
     */
    public static function fix($debug = false, $accountId, $phoneId = null, $phoneNumber = null, $smsService = null, $smsNumber = null)
    {
        $account = Account::findIdentity($accountId);
        $account->deleteCookieFile();

        $account->prepare(false);

        $phone = null;

        if ($phoneId != null && $phoneNumber != null && $smsService != null) {

            $phone = [
                'id' => $phoneId,
                'number' => $phoneNumber,
            ];

            $smsServiceModel = new BasicSmsServiceModel(false, $phone, $smsNumber, $smsService);
        }

        try {
            $account->login(true);
        } catch (InstagramException $e) {

            $account->prepareCurl();

            $discover = new Discover($account);
            $discover->identityCheckpointTypeByResponse($e->getResponse());

            if ($discover->isSms()) {

                $checkpointSms = new CheckpointSms($account, $discover->getHTML(), $discover->getCheckpointUrl(), $discover->getCheckpointType(), $debug, $phone, $smsNumber, $smsService);
                $checkpointSms->doCheckpoint();

                try {
                    $account->login(true);
                } catch (InstagramException $e) {

                    $account->working = 0;
                    $account->error_type = Discover::ERROR_EMAIL;
                    $account->save();

                    return;
                }

                $selfInfo = $account->instagram->people->getSelfInfo();

                // Удаляем номер телефона с профиля
                if ($selfInfo->hasUser()) {
                    $response = $account->instagram->account->setNameAndPhone(
                        $selfInfo->getUser()->getFullName() == null ? '' : $selfInfo->getUser()->getFullName(),
                        ''
                    );

                    if ($response->isOk()) {
                        $account->working = 1;
                        $account->error_type = null;
                        $account->save();

                        return;
                    }
                }

                if (isset($smsServiceModel)) {
                    $smsServiceModel->cancel();
                }

                throw new CheckpointException("Не сумели отредактировать аккаунт после починки");
            } else {

                if (isset($smsServiceModel)) {
                    $smsServiceModel->cancel();
                }

                $account->error_type = Discover::ERROR_UNKNOWN;
                $account->save();

                throw new CheckpointException("Аккаунт не требует подтверждения смс");
            }
        }
    }
}