<?php

namespace app\models\Instagram\checkpoint;

use app\models\helpers\ConsoleHelpers;
use app\models\helpers\ParserHelper;
use app\models\Instagram\Account;
use app\models\Instagram\checkpoint\Exceptions\CheckpointException;
use app\models\Instagram\SmsServices\BasicSmsServiceModel;
use app\models\Instagram\SmsServices\Exceptions\SmsServiceException;

class CheckpointSms
{
    CONST HOST_I = 'i';
    CONST HOST_WWW = 'www';

    private $host = self::HOST_WWW;

    private $checkpointUrl;

    private $html;

    private $checkpointType;

    private $smsServiceModel;

    private $debug;

    private $phone;

    private $curl;

    private $account;

    private $smsNumber;

    /**
     * CheckpointSms constructor.
     * @param Account $account
     * @param $html
     * @param $checkpointUrl
     * @param $checkpointType
     * @param bool $debug
     * @param $phone
     * @param int $smsNumber
     * @param null $smsService
     */
    public function __construct(Account $account, $html, $checkpointUrl, $checkpointType, $debug = false, $phone = null, $smsNumber = 1, $smsService = null)
    {
        $this->html = $html;
        $this->smsNumber = $smsNumber;
        $this->checkpointType = $checkpointType;
        $this->checkpointUrl = $checkpointUrl;
        $this->debug = $debug;
        $this->phone = $phone;
        $this->account = $account;

        $this->smsServiceModel = new BasicSmsServiceModel($debug, $phone, $smsNumber, $smsService);
    }

    public function doCheckpoint($host = self::HOST_WWW)
    {
        $this->host = $host;

        if ($this->host == self::HOST_WWW) {
            $this->checkpointUrl = str_replace('i.instagram', 'www.instagram', $this->checkpointUrl);
        }

        if ($this->phone == null) {
            if (false === $phone = $this->smsServiceModel->getNumber()) {
                throw new SmsServiceException("Не смогли получить номер в " . $this->smsServiceModel->getSmsService());
            }
        }

        $this->checkpointFirstStep();

        $form = $this->checkpointSecondStep();

        $this->checkpointThirdStep($form);
    }

    public function checkpointFirstStep()
    {
        if ($this->checkpointType == Discover::ERROR_SMS || $this->checkpointType == Discover::ERROR_SMS_GO_BACK) {
            $formData = ParserHelper::getFormData($this->html, 0, [
                'phone_number' => $this->smsServiceModel->phone['number'],
            ]);
        } elseif ($this->checkpointType == Discover::ERROR_SMS_ADD_PHONE) {

            preg_match('/"csrf_token":\s*"([A-Za-z0-9^"]*)"/iu', $this->html, $found);

            if (isset($found[1])) {
                $csrftoken = $found[1];
            } else {
                throw new CheckpointException("Не смогли найти csrftoken");
            }

            preg_match('/"forward":\s*"(\/challenge\/[0-9]*[\/]*[A-Za-z0-9]*[\/]*)"/iu', $this->html, $found);

            if (isset($found[1])) {
                $action = $found[1];
            } else {
                throw new CheckpointException("Не смогли найти forward");
            }

            $formData = [
                'action' => $action,
                'data' => [
                    'phone_number' => $this->smsServiceModel->phone['number'],
                    'csrfmiddlewaretoken' => $csrftoken,
                ],
            ];

        } else {
            throw new CheckpointException("Неизвестный параметр checkpointType");
        }

        if ($formData == null) {
            throw new SmsServiceException("Пустая форма");
        }

        if (isset($formData['data']) && isset($formData['data']['security_code'])) {

            if ($this->debug)
                ConsoleHelpers::log('Нажимаем кнопку "Go back"', 36);

            $formData = ParserHelper::getFormData($this->html, 1);

            $this->account->curl->setOpt(CURLOPT_FOLLOWLOCATION, true);

            $this->account->curl->setReferer($this->checkpointUrl);
            $this->account->curl->setHeader('x-csrftoken', $formData['data']['csrfmiddlewaretoken']);

            $html = $this->account->curl->post("https://{$this->host}.instagram.com" . $formData['action'], $formData['data']);

            $this->checkpointUrl = $formData['action'];

            if (preg_match('/Enter your phone number/iu', $html)) {

                if ($this->debug)
                    ConsoleHelpers::log('Успешно нажали "Go back" и теперь аккаунт требует ввести номер телефона', 36);

                $this->html = $html;

                $formData = ParserHelper::getFormData($html);

                $formData['data']['phone_number'] = $this->smsServiceModel->phone['number'];
            } else {
                throw new CheckpointException('После нажатия "Go back" произошла ошибка');
            }
        }

        $this->account->curl->setReferer($this->checkpointUrl);
        $this->account->curl->setHeader('x-csrftoken', $formData['data']['csrfmiddlewaretoken']);

        $this->html = $this->account->curl->post("https://{$this->host}.instagram.com" . $formData['action'], $formData['data']);

        if (preg_match('/please choose a different phone number/iu', $this->html)) {
            $this->account->banned = 1;
            $this->account->save();

            $this->smsServiceModel->cancel();

            throw new SmsServiceException("Instagram просит другой телефон");
        }

        if (!preg_match('/Enter Security Code/iu', $this->html)) {
            $this->smsServiceModel->cancel();
            throw new CheckpointException("Не смогли передать Instagram номер телефона");
        }
    }

    public function checkpointSecondStep()
    {
        $code = $this->smsServiceModel->getCode();

        $formData = ParserHelper::getFormData($this->html, 0, [
            'security_code' => $code,
        ]);

        $formData['action'] = "https://{$this->host}.instagram.com" . $formData['action'];

        return $formData;
    }

    public function checkpointThirdStep($formData)
    {
        $this->account->curl->setReferer($this->checkpointUrl);
        $this->account->curl->post($formData['action'], $formData['data']);

        if (!preg_match('/dismiss/', $this->account->curl->response)) {
            throw new CheckpointException("Ошибка на последнем шаге");
        }

        if ($this->debug)
            ConsoleHelpers::log("Отправили код", 32);

        // Если номер используется больше 1-го раза, то отмечаем его успешным
        if ($this->smsNumber > 1) {
            $this->smsServiceModel->success();
        }
    }
}