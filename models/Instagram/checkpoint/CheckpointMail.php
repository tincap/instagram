<?php

namespace app\models\Instagram\checkpoint;

use app\models\helpers\ConsoleHelpers;
use app\models\helpers\ParserHelper;
use app\models\Instagram\Account;
use app\models\Instagram\checkpoint\Exceptions\CheckpointException;
use app\models\Mail\Exceptions\MailLoginException;
use app\models\Mail\Message;

class CheckpointMail
{
    /** @var Account $account */
    public $account;

    /** @var bool */
    public $debug;

    public $html;

    public $checkpointUrl;

    public $errorMessage = '';

    public $checkpointVersion = 1;

    /**
     * CheckpointMail constructor.
     * @param $account
     * @param $html
     * @param $checkpointUrl
     * @param bool $debug
     */
    public function __construct($account, $html, $checkpointUrl, $debug = false)
    {
        $this->checkpointUrl = $checkpointUrl;
        $this->html = $html;
        $this->account = $account;
        $this->debug = $debug;
    }

    public function doCheckpoint()
    {
        if ($this->checkpointThird($this->checkpointSecondStep($this->checkpointFirstStep()))) {
            return true;
        }

        return false;
    }

    /**
     * Страница нажатия на кнопку "Подтвердить по эл.почте"
     *
     * @return array|\stdClass
     * @throws CheckpointException
     */
    public function checkpointFirstStep()
    {
        $form = ParserHelper::getFormData($this->html);

        if ($form == null) {
            if (preg_match('/"back": "([^"]*)"/', $this->html, $found)) {

                if ($this->debug)
                    echo "Нажимаем go_back\n";

                $form = [];

                if (isset($found[1])) {
                    $form['action'] = $found[1];

                    $this->account->curl->setOpt(CURLOPT_FOLLOWLOCATION, true);
                    $this->account->curl->setReferer('https://i.instagram.com' . $form['action']);
                    $this->account->curl->setHeader('x-csrftoken', $this->account->curl->getCookie('csrftoken'));

                    $this->html = $this->account->curl->post('https://i.instagram.com' . $form['action']);

                    if (preg_match('/send you a security code to verify your account/iu', $this->html)) {
                        $form = ParserHelper::getFormData($this->html);
                    }
                }
            }
        }

        if ($form == null) {
            if (preg_match('/"forward": "([^"]*)"/', $this->html, $found)) {

                $form = [];

                if (isset($found[1])) {
                    if ($this->debug)
                        ConsoleHelpers::log("forward: $found[1]", 37);

                    $form['action'] = $found[1];
                    $form['data']['choice'] = 1;
                }
            } else {
                throw new CheckpointException("Не смогли найти forward");
            }
        }

        if (isset($form['data']['security_code'])) {

            if ($this->debug)
                ConsoleHelpers::log('Нажимаем кнопку "Go back"', 37);

            $form = ParserHelper::getFormData($this->html, 1);

            $this->account->curl->setOpt(CURLOPT_FOLLOWLOCATION, true);
            $this->account->curl->setReferer($this->checkpointUrl);
            $this->html = $this->account->curl->post('https://i.instagram.com' . $form['action'], $form['data']);

            $this->checkpointUrl = $form['action'];

            if (preg_match('/send you a security code to verify your account/iu', $this->html)) {
                $form = ParserHelper::getFormData($this->html);
            }
        }

        return $form;
    }

    /**
     * Нажимаем на кнопку "Подтвердить по эл.почте" и получаем код
     *
     * @param $formData
     * @return array|\stdClass
     * @throws CheckpointException
     * @throws MailLoginException
     */
    public function checkpointSecondStep($formData)
    {
        if ($formData instanceof \stdClass) {
            throw new CheckpointException($formData->error);
        }

        $message = null;

        if (!isset($formData['data']['security_code'])) {
            if (!isset($formData['data']['choice']) && !isset($formData['data']['email'])) {
                throw new CheckpointException("Аккаунт не требует подтерждения email");
            }

            if (isset($formData['data']['choice']) && $formData['data']['choice'] == 0) {
                $this->account->banned = 1;
                $this->account->working = 0;
                $this->account->save();

                throw new CheckpointException("Аккаунт требует подтверждения с помощью SMS. Его невозможно восстановить.");
            }

            /** @var \app\models\mail\Account $mail */
            $mail = $this->account->mail;

            $mail->login();

            $message = new Message($mail);
            $message->clearMessages();

            $this->account->curl->setOpt(CURLOPT_FOLLOWLOCATION, true);
            $this->account->curl->setReferer($this->checkpointUrl);
            $this->account->curl->setHeader('x-csrftoken', $this->account->curl->getCookie('csrftoken'));
            $this->html = $this->account->curl->post('https://i.instagram.com' . $formData['action'], $formData['data']);
        }

        if ($this->debug)
            echo "Mail авторизован\nЖдем код";

        $attempt = 0;

        do {
            sleep(10);

            $attempt++;

            $code = $message->getVerifyCode();

            if ($this->debug)
                echo ".";

        } while (is_null($code) && $attempt < 15);

        if ($this->debug)
            echo "\n";

        if (is_null($code)) {
            throw new CheckpointException("Код подтверждения не пришел на почту");
        }

        if ($this->debug)
            ConsoleHelpers::log("Код: $code", 32);

        $message->clearMessages();

        if ($this->checkpointVersion == 1 && !isset($formData['data']['security_code'])) {
            $formData = ParserHelper::getFormData($this->html, 0, [
                'security_code' => $code,
            ]);
        }

        return $formData;
    }

    /**
     * Вводим код и нажимаем на кнопку "ОК"
     *
     * @param $formData
     * @return bool
     * @throws CheckpointException
     */
    public function checkpointThird($formData)
    {
        if (!is_array($formData)) {
            throw new CheckpointException("Не смогли починить аккаунт. formData не вернул массив");
        }

        $this->account->curl->setOpt(CURLOPT_FOLLOWLOCATION, true);
        $this->account->curl->setReferer($this->checkpointUrl);
        $this->account->curl->setHeader('x-csrftoken', $this->account->curl->getCookie('csrftoken'));

        $this->html = $this->account->curl->post('https://i.instagram.com' . $formData['action'], $formData['data']);

        if (preg_match('/>Was This You/iu', $this->html)) {

            if ($this->debug)
                ConsoleHelpers::log("Instagram спрашивает, ты ли это", 37);

            $formData = ParserHelper::getFormData($this->html);

            $formData['data']['choice'] = '0';

            $this->account->curl->setOpt(CURLOPT_FOLLOWLOCATION, true);
            $this->account->curl->setReferer($this->checkpointUrl);
            $this->account->curl->setHeader('x-csrftoken', $this->account->curl->getCookie('csrftoken'));
            $this->html = $this->account->curl->post('https://i.instagram.com' . $formData['action'], $formData['data']);
        }

        if (preg_match('/this code has expired/iu', $this->html)) {

            $mail = $this->account->mail;
            $mail->login();

            $message = new Message($mail);
            $message->clearMessages();

            throw new CheckpointException("Код устарел. Удалили сообщения с mail. Попробуй починить еще раз.");
        }

        if (preg_match('/(Аккаунт подтвержден)|(Your account has been verified)|(checkpoint\/dismiss)/iu', $this->html)) {
            return true;
        }

        if (preg_match('/Please check the code we sent you and try again/iu', $this->html)) {
            throw new CheckpointException("Неверный код");
        }

        if ($this->debug)
            echo $this->html;

        throw new CheckpointException("После всех действий аккаунт не подтвержден");
    }
}