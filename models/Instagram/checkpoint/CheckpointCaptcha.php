<?php

namespace app\models\Instagram\checkpoint;

use app\components\AntiCaptcha;
use app\models\helpers\ConsoleHelpers;
use app\models\helpers\ParserHelper;
use app\models\Instagram\Account;
use app\models\Instagram\checkpoint\Exceptions\CheckpointException;
use Curl\Curl;

class CheckpointCaptcha
{
    /** @var Account $account */
    private $account;

    /** @var bool */
    private $debug;

    private $html;

    private $checkpointUrl;

    private $csrfmiddlewaretoken;

    private $captchaText;

    private $recaptcha_challenge_field;

    public function __construct(Account $account, $html, $checkpointUrl, $debug = false)
    {
        $this->account = $account;
        $this->checkpointUrl = $checkpointUrl;
        $this->debug = $debug;
        $this->html = $html;
    }

    /**
     * @return string
     */
    public function getRecaptchaChallengeField()
    {
        return $this->recaptcha_challenge_field;
    }

    /**
     * @return string
     */
    public function getCsrfmiddlewaretoken()
    {
        return $this->csrfmiddlewaretoken;
    }

    /**
     * @return string
     */
    public function getCaptchaText()
    {
        return $this->captchaText;
    }

    public function doCheckpoint()
    {
        $this->checkpointFirstStep();

        $formData = ParserHelper::getFormData($this->html, 0, [
            'recaptcha_response_field' => $this->captchaText,
            'csrfmiddlewaretoken' => $this->csrfmiddlewaretoken,
        ]);

        unset($formData['data']['submit']);

        $this->account->curl->setReferer($this->checkpointUrl);
        $this->account->curl->setHeader('upgrade-insecure-requests', '1');

        $this->account->curl->post($this->checkpointUrl, $formData['data']);

        if ($this->account->curl->httpStatusCode == 200) {
            return true;
        }

        throw new CheckpointException("Запрос вернул код " . $this->account->curl->httpStatusCode);
    }

    /**
     * @return false|string
     * @throws CheckpointException
     */
    public function checkpointFirstStep()
    {
        $formData = ParserHelper::getFormData($this->html);

        if (!isset($formData['data']['csrfmiddlewaretoken'])) {
            throw new CheckpointException("Не смогли найти csrfmiddlewaretoken на странице Google");
        }
        $this->csrfmiddlewaretoken = $formData['data']['csrfmiddlewaretoken'];

        preg_match('/<iframe src="([^"]*)" height="300" width="500" frameborder="0">/iu', $this->html, $found);

        if (!isset($found[1])) {
            throw new CheckpointException("Не нашли iframe");
        }

        // Страница капчи Google
        $url = $found[1];

        $curl = new Curl();
        $this->html = $curl->get($url);

        if (!preg_match('/We need to make sure you are a human/iu', $this->html)) {
            throw new CheckpointException("Это не странца капчи Google");
        }

        // Ищем изображение капчи
        preg_match('/<img width="300" height="57" alt="" src="([^"]*)">/iu', $this->html, $found);

        if (!isset($found[1])) {
            throw new CheckpointException("Не смогли найти изображение капчи на странце Google");
        }

        $imageLink = 'https://www.google.com/recaptcha/api/' . $found[1];

        if ($this->debug)
            ConsoleHelpers::log("Нашли изображение капчи. Разгадываем его.", 34);

        $antiCaptcha = new AntiCaptcha($imageLink, $this->account->username);

        $n = 0;

        do {
            $captchaText = $antiCaptcha->getTranscript();
            $n++;
        } while ($captchaText === false && $n < 5);

        $antiCaptcha->deleteCaptchaFile();

        if ($captchaText === false) {
            throw new CheckpointException("Произошла ошибка при разгадке капчи");
        }

        if ($this->debug)
            ConsoleHelpers::log("Капча: $captchaText", 32);

        $this->captchaText = $captchaText;

        $formData = ParserHelper::getFormData($this->html, 0);

        if (!isset($formData['data']['recaptcha_challenge_field'])) {
            throw new CheckpointException("Не смогли найти recaptcha_challenge_field на странице Google");
        }

        $this->recaptcha_challenge_field = $formData['data']['recaptcha_challenge_field'];

        return $captchaText;
    }
}