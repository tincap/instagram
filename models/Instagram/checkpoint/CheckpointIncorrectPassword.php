<?php

namespace app\models\Instagram\checkpoint;

use app\models\helpers\ParserHelper;
use app\models\Instagram\Account;
use app\models\Instagram\checkpoint\Exceptions\CheckpointException;
use InstagramAPI\Exception\InstagramException;

class CheckpointIncorrectPassword
{
    /** @var Account */
    private $account;

    private $debug;

    private $curl;

    private $html;

    private $checkpointPassword;

    private $checkpointUrl = 'https://www.instagram.com/accounts/password/reset/';

    public function __construct($account, $debug = false)
    {
        $this->account = $account;
        $this->debug = $debug;

        $this->checkpointPassword = new CheckpointPassword($this->account, true);
    }

    public function doCheckpoint()
    {
        if ($this->account->checkChangedMail()) {
            throw new CheckpointException("Email угнан");
        }

        $this->checkpointFirstStep();

        $this->checkpointSecondStep();

        try {
            $this->account->login(true);
            return true;
        } catch (InstagramException $e) {
            throw new CheckpointException("Не смог авторизоваться после починки");
        }

    }

    public function checkpointFirstStep()
    {
        $this->html = $this->account->curl->get($this->checkpointUrl);

        $checkpointCaptcha = new CheckpointCaptcha($this->account, $this->html, $this->checkpointUrl, true);
        $checkpointCaptcha->checkpointFirstStep();

        $formData = ParserHelper::getFormData($this->html, 0, [
            'recaptcha_response_field' => $checkpointCaptcha->getCaptchaText(),
            'email_or_username' => $this->account->mail->username,
            'csrfmiddlewaretoken' => $checkpointCaptcha->getCsrfmiddlewaretoken(),
            'recaptcha_challenge_field' => $checkpointCaptcha->getRecaptchaChallengeField(),
        ]);

        unset($formData['data']['0']);

        $this->account->curl->setOpt(CURLOPT_FOLLOWLOCATION, true);
        $this->account->curl->setReferer($this->checkpointUrl);
        $this->account->curl->setHeader('upgrade-insecure-requests', '1');
        $html = $this->account->curl->post($this->checkpointUrl, $formData['data']);

        if (preg_match('/for a link to reset your password/iu', $html)) {
            return true;
        } else {
            if (preg_match('/That e-mail address or username doesn/iu', $html)) {

                $this->account->error_type = Discover::ERROR_CHANGED_EMAIL;
                $this->account->banned = 1;
                $this->account->working = 0;
                $this->account->save();

                throw new CheckpointException("Пользователя с таким email не существует");
            } elseif (preg_match('/this user is not active/iu', $html)) {

                $this->account->error_type = Discover::ERROR_DISABLED;
                $this->account->banned = 1;
                $this->account->working = 0;
                $this->account->save();

                throw new CheckpointException("Аккаунт забанен");
            } else {
                throw new CheckpointException('Неизвестная ошибка после нажатия кнопки "Сбросить пароль"');
            }
        }
    }

    public function checkpointSecondStep()
    {
        return $this->checkpointPassword->checkpointSecondStep($this->checkpointPassword->checkpointFirstStep());
    }

}