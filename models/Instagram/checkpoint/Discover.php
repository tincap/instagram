<?php

namespace app\models\Instagram\checkpoint;

use app\models\Instagram\Account;
use Curl\Curl;

/**
 * Класс для определения вида checkpoint
 * @package app\models\Instagram\checkpoint
 */
class Discover
{
    CONST ERROR_SMS = 'sms';
    CONST ERROR_SMS_ADD_PHONE = 'sms_add_phone';
    CONST ERROR_SMS_GO_BACK = 'sms_go_back';
    CONST ERROR_EMAIL = 'email';
    CONST ERROR_PASSWORD = 'password';
    CONST ERROR_INCORRECT_USERNAME = 'incorrect_username';
    CONST ERROR_DISABLED = 'disabled';
    CONST ERROR_INCORRECT_PASSWORD = 'incorrect_password';
    CONST ERROR_CHANGED_EMAIL = 'changed_email';
    CONST ERROR_BLOCKED_EMAIL = 'blocked_email';
    CONST ERROR_CAPTCHA = 'captcha';
    CONST ERROR_FEEDBACK = 'feedback';
    CONST ERROR_UNKNOWN = 'unknown';

    /** @var Account */
    private $account;

    /** @var string */
    private $html;

    /** @var string */
    private $checkpointUrl;

    /** @var string */
    private $checkpointType;

    /**
     * Discover constructor.
     * @param $account
     */
    public function __construct($account)
    {
        $this->account = $account;

        if (!($this->account->curl instanceof Curl)) {
            $this->account->prepareCurl();
        }
    }

    /**
     * @return string
     */
    public function getCheckpointType()
    {
        return $this->checkpointType;
    }

    /**
     * @return string
     */
    public function getHTML()
    {
        return $this->html;
    }

    /**
     * @return string
     */
    public function getCheckpointUrl()
    {
        return $this->checkpointUrl;
    }

    /**
     * @return bool
     */
    public function isSms()
    {
        if ($this->checkpointType == self::ERROR_SMS || $this->checkpointType == self::ERROR_SMS_GO_BACK || $this->checkpointType == self::ERROR_SMS_ADD_PHONE) {
            return true;
        }

        return false;
    }

    public function isBanned()
    {
        if ($this->checkpointType == self::ERROR_INCORRECT_USERNAME || $this->checkpointType == self::ERROR_DISABLED) {
            return true;
        }

        return false;
    }

    /**
     * По Response определяет вид ошибки у аккаунта
     *
     * @param $response mixed
     * @return null|string
     */
    public function identityCheckpointTypeByResponse($response)
    {
        if ($response == null) {
            $this->checkpointType = self::ERROR_UNKNOWN;
            return $this->checkpointType;
        }

        if ($response->hasErrorType()) {

            // Неверный пароль
            if (preg_match("/The password you entered is incorrect|bad_password/i", $response->getErrorType())) {
                $this->checkpointType = self::ERROR_INCORRECT_PASSWORD;
                return $this->checkpointType;
            }

            // Сброшен пароль
            if (preg_match("/unusable_password/i", $response->getErrorType())) {
                $this->checkpointType = self::ERROR_PASSWORD;
                return $this->checkpointType;
            }

            // Забанен или неверное имя пользователя
            if ($response->hasMessage() && preg_match('/The username you entered doesn|Your account has been disabled/iu', $response->getMessage())) {

                if (preg_match('/The username you entered doesn/iu', $response->getMessage())) {
                    $this->checkpointType = self::ERROR_INCORRECT_USERNAME;
                    return $this->checkpointType;
                }

                if (preg_match('/Your account has been disabled/iu', $response->getMessage())) {
                    $this->checkpointType = self::ERROR_DISABLED;
                    return $this->checkpointType;
                }
            }
        }

        if ($response->hasChallenge()) {
            if (is_array($response->getChallenge())) {
                $this->checkpointUrl = $response->getChallenge()['url'];
            } else {

                if ($response->getChallenge() == null || $response->getChallenge() == 0) {
                    $this->account->deleteCookieFile();
                    return null;
                }

                $this->checkpointUrl = $response->getChallenge()->getUrl();
            }

            $this->html = $this->account->curl->get($this->checkpointUrl);

            return $this->identityCheckpointTypeByHtml($this->html);
        }

        $this->account->deleteCookieFile();
        return null;
    }

    /**
     * По HTML определяет вид ошибки у аккаунта
     *
     * @param $html string
     * @return string
     */
    public function identityCheckpointTypeByHtml($html)
    {
        if (preg_match('/(enter your phone number)/iu', $html)) {
            $this->checkpointType = self::ERROR_SMS;
            return $this->checkpointType;
        } elseif (preg_match('/phone number ending/iu', $html)) {
            // Надо нажать кнопку "Go back"
            $this->checkpointType = self::ERROR_SMS_GO_BACK;
            return $this->checkpointType;

        } elseif (preg_match('/(SubmitPhoneNumberForm)|(VerifySMSCodeForm)/iu', $html)) {
            $this->checkpointType = self::ERROR_SMS_ADD_PHONE;
            return $this->checkpointType;

        } elseif (preg_match('/(send you a security code to verify your account)|(Enter Security Code)|(SelectVerificationMethodForm)|(contact_point)|(SelectContactPointRecoveryForm)|(ReviewLoginForm)/iu', $html)) {
            $this->checkpointType = self::ERROR_EMAIL;
            return $this->checkpointType;
        } elseif (preg_match('/Please enter the text below to verify your account and continue using Instagram/i', $html)) {
            $this->checkpointType = self::ERROR_CAPTCHA;
            return $this->checkpointType;
        }

        $this->account->deleteCookieFile();
        return null;
    }
}