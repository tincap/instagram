<?php

namespace app\models\Instagram;

use app\models\App\User;
use app\models\Instagram\Exceptions\MobileProxyException;
use Curl\Curl;

/**
 * @property string update_url
 */
class MobileProxy extends Proxy
{
    public function discoverIp()
    {
        $curl = new Curl();

        $curl->setOpt(CURLOPT_HTTPPROXYTUNNEL, 1);
        $curl->setOpt(CURLOPT_PROXY, $this->ip);
        $curl->setOpt(CURLOPT_PROXYUSERPWD, $this->password);

        $html = $curl->get("https://2ip.ru");

        preg_match("/\d+\.\d+\.\d+\.\d+/", $html, $found);

        if (isset($found[0])) {
            return $found[0];
        }

        throw new MobileProxyException("Не смогли узнать IP");
    }

    public function updateProxy()
    {
        $curl = new Curl();
        $html = $curl->get(trim($this->update_url));

        if (preg_match('/OK/', $html)) {
            return true;
        }

        throw new MobileProxyException("Не смогли обновить IP");
    }

    /** @inheritdoc */
    public static function tableName()
    {
        return 'mobile-proxy-' . User::getActiveUser();
    }
}