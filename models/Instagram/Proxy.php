<?php

namespace app\models\Instagram;

use app\models\App\User;
use Curl\Curl;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "proxy".
 *
 * @property integer $id
 * @property string  $ip
 * @property string  $password
 */
class Proxy extends ActiveRecord
{
    /** @var Curl */
    public $curl;

    /**
     * Поиск по ip
     *
     * @param $ip
     * @return static
     */
    public static function findByIp($ip)
    {
        return static::findOne(['ip' => $ip]);
    }

    /**
     * Поиск по id
     *
     * @param $id
     * @return static
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /** @inheritdoc */
    public static function tableName()
    {
        return 'proxy-' . User::getActiveUser();
    }
}