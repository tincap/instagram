<?php

namespace app\models\Mail;

use app\models\App\User;
use app\models\helpers\MailSignatureUtils;
use app\models\Instagram\checkpoint\Discover;
use app\models\Mail\Exceptions\MailLoginException;
use Curl\Curl;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "mail-account".
 *
 * @property integer $id
 * @property string  $username
 * @property string  $password
 * @property string  $user_agent
 *
 * @property mixed proxy
 */
class Account extends ActiveRecord
{
    /** @var Curl */
    public $curl;

    public function prepareCurl()
    {
        $this->curl = new Curl();

        if (empty($this->user_agent)) {
            $this->user_agent = MailSignatureUtils::generateUserAgent();
            $this->save();
        }

        $headers = [
            'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Content-Type: application/x-www-form-urlencoded; charset=UTF-8',
            'Accept-Language: en-US,en;q=0.8',
            'Connection: keep-alive',
            'Cache-Control: max-age=0',
        ];

        $this->curl->setOpt(CURLOPT_HTTPHEADER, $headers);

        $proxy = $this->proxy;

        $this->curl->setOpt(CURLOPT_HTTPPROXYTUNNEL, 1);
        $this->curl->setOpt(CURLOPT_PROXY, $proxy->ip);
        $this->curl->setOpt(CURLOPT_PROXYUSERPWD, $proxy->password);

        $this->curl->setCookieFile($this->getCookieFile());
        $this->curl->setUserAgent($this->user_agent);
    }

    public function login()
    {
        $this->prepareCurl();

        if (!$this->isLoggedIn()) {

            $url = 'https://auth.mail.ru/cgi-bin/auth?rand=129146199';

            $this->curl->setOpt(CURLOPT_FOLLOWLOCATION, true);

            $data = [
                'post' => '',
                'mhost' => 'm.mail.ru',
                'login_from' => '',
                'Login' => explode('@', $this->username)[0],
                'Domain' => $this->getDomain(),
                'Password' => $this->password,
            ];

            $this->curl->setCookieJar($this->getCookieFile());
            $response = $this->curl->post($url, $data);

            $this->curl->close();

            $this->prepareCurl();

            if ($this->isLoggedIn()) {
                return true;
            }
        } else {
            return true;
        }

        if ($response == '') {
            throw new MailLoginException("Mail аккаунта не смог авторизоваться. Ошибка прокси! Прокси #" . $this->proxy->id);
        }

        if (preg_match('/Восстановление пароля/iu', $response)) {

            /** @var \app\models\Instagram\Account $account */
            $account = \app\models\Instagram\Account::find()->where("email_id = {$this->id}")->one();
            $account->working = 0;
            $account->error_type = Discover::ERROR_BLOCKED_EMAIL;
            $account->banned = 1;
            $account->save();

            throw new MailLoginException("Mail аккаунта заблокирован");
        }

        throw new MailLoginException("Mail аккаунта не смог авторизоваться");
    }

    /**
     * Возвращает домен аккаунта
     *
     * @return mixed
     */
    public function getDomain()
    {
        return explode('@', $this->username)[1];
    }

    /**
     * Проверяет залогининен ли пользователь
     *
     * @return bool
     */
    public function isLoggedIn()
    {
        if ($this->existsCookieFile()) {

            $data = $this->getCookieData();

            if (isset($data['GarageID']) &&
                isset($data['Mpop']) &&
                isset($data['ssdc']) &&
                isset($data['ssdc_info']) &&
                isset($data['t'])) {
                return true;
            }
        }
        return false;
    }

    /**
     * Парсит куки-файл и возвращает данные
     *
     * @return array|bool
     */
    public function getCookieData()
    {
        if (!$this->existsCookieFile()) {
            return [];
        }

        $cookieFile = $this->getCookieFile();

        $handle = fopen($cookieFile, 'r');

        $out = [];

        while (!feof($handle)) {
            $buffer = fgets($handle);

            preg_match('/\s[FALSE|TRUE]+\t\/\t[FALSE|TRUE]+\t[0-9]+\t([^\t]*)\t(.*)/', $buffer, $found);

            if (isset($found[1]) && isset($found[2])) {
                $out[$found[1]] = $found[2];
            }
        }

        fclose($handle);

        return $out;
    }

    /**
     * Возвращает файл куки
     *
     * @return bool|string
     */
    public function getCookieFile()
    {
        return \Yii::getAlias('@app/data/Mail/cookies/' . User::getActiveUser() . '/' . $this->id . '.txt');
    }

    /**
     * Проверяет на существование куки-файла
     *
     * @return bool
     */
    public function existsCookieFile()
    {
        return (bool) file_exists($this->getCookieFile());
    }

    /**
     * Proxy
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProxy()
    {
        return static::hasOne(Proxy::className(), ['id' => 'proxy_id']);
    }

    /**
     * Поиск по username
     *
     * @param $username
     * @return static
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * Поиск по id
     *
     * @param $id
     * @return static
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /** @inheritdoc */
    public static function tableName()
    {
        return 'mail-account-' . User::getActiveUser();
    }
}