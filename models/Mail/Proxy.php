<?php

namespace app\models\Mail;

/**
 * This is the model class for table "mail-proxy".
 *
 * @property integer $id
 * @property string  $ip
 * @property string  $password
 */
class Proxy extends \app\models\Instagram\Proxy
{
    /** @inheritdoc */
    public static function tableName()
    {
        return 'mail-proxy';
    }
}