<?php

namespace app\models\Mail;

use app\components\simple_html_dom;
use app\models\helpers\ParserHelper;

class Message
{
    /** @var Account */
    public $account;

    /**
     * Message constructor.
     * @param Account $account
     */
    public function __construct(Account $account)
    {
        $this->account = $account;
    }

    /**
     * Вернуть список сообщений
     *
     * @param int $page
     * @return array
     */
    public function getMessagesList($page = 1)
    {
        $url = \Yii::$app->params['mail']['host'] . "messages/inbox/?page=$page";

        $this->account->curl->setOpt(CURLOPT_FOLLOWLOCATION, true);
        $html = $this->account->curl->get($url);

        $dom = new simple_html_dom();
        $dom->load($html);

        $boxes = $dom->find('.messageline');

        $messages = [];

        foreach ($boxes as $box) {
            $from    = $box->find('.messageline__from', 0);
            $subject = $box->find('.messageline__subject', 0);
            $link    = $box->find('.messageline__link', 0);

            $messages[] = [
                'id' => explode('/', $link->href)[2],
                'from' => trim($from->innertext),
                'subject' => trim($subject->innertext),
            ];
        }

        return $messages;
    }

    /**
     * Возвращает список id сообщений
     *
     * @param int $page
     * @param string $url
     * @return array
     */
    public function getMessagesIdList($page = 1, $url = 'messages/inbox')
    {
        $url = \Yii::$app->params['mail']['host'] . $url . "/?page=$page";

        $this->account->curl->setOpt(CURLOPT_FOLLOWLOCATION, true);
        $html = $this->account->curl->get($url);

        $dom = new simple_html_dom();
        $dom->load($html);

        $boxes = $dom->find('.messageline');

        $idList = [];

        foreach ($boxes as $box) {
            $link    = $box->find('.messageline__link', 0);
            $idList[] = explode('/', $link->href)[2];
        }

        return $idList;
    }

    /**
     * Удалить сообщения переданные в массив
     *
     * @param array $messages
     * @return bool
     */
    public function deleteMessages(array $messages)
    {
        $url = \Yii::$app->params['mail']['host'] . '/messages/inbox';

        $html = $this->account->curl->get($url);

        $formData = ParserHelper::getFormData($html, 0);

        unset($formData['data']['id']);
        unset($formData['data']['spamabuse']);
        unset($formData['data']['move_folderselect']);

        $formData['data']['remove'] = '';

        $postStr = http_build_query($formData['data'], '', '&');

        foreach ($messages as $message) {
            $postStr .= '&id=' . $message;
        }

        $this->account->curl->post(\Yii::$app->params['mail']['host'] . ltrim($formData['action'], '/') . '/', $postStr);

        if ($this->account->curl->httpStatusCode == 302) {
            return true;
        }

        return false;
    }

    public function clearTrash()
    {
        $url = \Yii::$app->params['mail']['host'] . '/messages/trash';

        $html = $this->account->curl->get($url);

        $messageList = $this->getMessagesIdList(1, '/messages/trash');

        if (count($messageList) == 0) {
            return true;
        }

        $formData = ParserHelper::getFormData($html, 0, [
            'confirm' => 'on',
            'remove' => '1',
            'next' => '',
        ]);

        if ($formData instanceof \stdClass) {
            return $formData->error;
        }

        unset($formData['data']['id']);

        $postStr = http_build_query($formData['data'], '', '&');

        foreach ($messageList as $message) {
            $postStr .= '&id=' . $message;
        }

        sleep(1);
        $this->account->curl->post(\Yii::$app->params['mail']['host'] . $formData['action'], $postStr);

        return true;
    }

    /**
     * Найти и вернуть id сообщения Instagram о верификации
     *
     * @return null
     */
    public function getInstagramVerifyMessageId()
    {
        $messages = $this->getMessagesList();

        foreach ($messages as $message) {
//            if (preg_match('/Verify Your Account|Подтвердите свой аккаунт|Підтвердьте свій обліковий запис|Vérifier votre compte|Zweryfikuj konto/iu',  $message['subject']) && preg_match('/instagram/iu',  $message['from'])) {
//                return $message['id'];
//            }

            if (preg_match('/instagram/iu',  $message['from'])) {
                return $message['id'];
            }
        }

        return null;
    }

    /**
     * Показать сообщение
     *
     * @param $id
     * @return string
     */
    public function showMessage($id)
    {
        $html = $this->account->curl->get(\Yii::$app->params['mail']['host'] . "/message/$id");

        $dom = new simple_html_dom();
        $dom->load($html);

        return $dom->find('.readmsg__text-container', 9)->innertext;
    }

    /**
     * Возвращает код инстаграмма с последнего сообщения
     *
     * @return null
     */
    public function getVerifyCode()
    {
        $id = $this->getInstagramVerifyMessageId();

        if (is_null($id)) {
            return null;
        }

        $messageText = $this->showMessage($id);

        preg_match('/Your Instagram security code is (\d+)|Ваш код безопасности Instagram (\d+)|<font size="6">(\d+)<\/font>/iu', $messageText, $found);

        if (!empty($found[1])) {
            return (string) $found[1];
        } else {
            if (!empty($found[2])) {
                return (string) $found[2];
            } else {
                if (isset($found[3])) {
                    return (string) $found[3];
                } else {
                    die("Не смогли найти код в сообщении");
                }
            }
        }
    }

    public function clearMessages()
    {
        $this->deleteMessages($this->getMessagesIdList());
    }
}