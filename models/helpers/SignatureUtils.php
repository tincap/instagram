<?php

namespace app\models\helpers;

class SignatureUtils
{
    public static function getDeviceData()
    {
        $csvfile = \Yii::getAlias('@app/data/files/system/devices.csv');

        $file_handle = fopen($csvfile, 'r');
        $line_of_text = [];
        while (!feof($file_handle)) {
            $line_of_text[] = fgetcsv($file_handle, 1024);
        }
        $deviceData = explode(';', $line_of_text[mt_rand(0, 11867)][0]);
        fclose($file_handle);

        return $deviceData;
    }

    public static function getRandomProcessor()
    {
        $processor = [
            'JDQ39',
            'MRA58K',
        ];

        return $processor[array_rand($processor)];
    }

    public static function getRandomAndroidVersion()
    {
        $androidVersion = [
            '4.2.1',
            '4.2.2',
            '4.4.3',
            '5.0.2',
            '5.1.1',
            '6.0.1',
            '7.0',
        ];

        return $androidVersion[array_rand($androidVersion)];
    }
}