<?php

namespace app\models\helpers;

class FileDbHelpers
{
    /**
     * Добавить в базу элемент
     *
     * @param $filename
     * @param $list
     */
    public static function add($list, $filename)
    {
        $fp = fopen($filename, "a+");

        if (is_array($list)) {
            foreach ($list as $member) {
                fwrite($fp, $member . "\n");
            }
        } else {
            if (is_string($list) || is_numeric($list)) {
                fwrite($fp, $list . "\n");
            }
        }

        fclose($fp);
    }

    /**
     * Перезаписывает базу
     *
     * @param $filename
     * @param $list
     */
    public static function overwrite($list, $filename)
    {
        $fp = fopen($filename, "w+");

        if (is_array($list)) {
            foreach ($list as $member) {
                fwrite($fp, $member . "\n");
            }
        } else {
            if (is_string($list) || is_numeric($list)) {
                fwrite($fp, $list . "\n");
            }
        }

        fclose($fp);
    }

    /**
     * Читать с базы
     *
     * @param $filename
     * @return array
     */
    public static function read($filename)
    {
        $memberList = [];

        $fp = fopen($filename, "r");

        while (!feof($fp)) {
            $uid = preg_replace("/\s/", "", fgets($fp, 50));

            if (strlen($uid) > 1) {
                $memberList[] = $uid;
            }
        }

        return $memberList;
    }

    /**
     * Проверяет на вхождение
     *
     * @param $item
     * @param $fileName
     * @return bool
     */
    public static function in($item, $fileName)
    {
        $itemList = self::read($fileName);

        if (in_array($item, $itemList)) {
            return true;
        }

        return false;
    }
}