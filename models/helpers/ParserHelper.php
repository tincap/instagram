<?php

namespace app\models\helpers;


use app\components\simple_html_dom;

class ParserHelper
{
    /**
     * Возвращает информацию и данные form на странице
     *
     * @param $html
     * @param int $formNumber
     * @param array $beforehandData - заранее заданные параметры
     * @return array|boolean
     */
    public static function getFormData($html, $formNumber = 0, $beforehandData = [])
    {
        $dom = new simple_html_dom();
        $dom->load($html);

        $form = $dom->find('form', $formNumber);

        if (!isset($form)) {
            return null;
        }

        $action = htmlspecialchars_decode($form->action);

        $inputs = $form->find('input');

        $data = [];

        foreach ($inputs as $input) {
            $data[$input->name] = html_entity_decode($input->value);
            if ($input->attr['type'] != 'submit') {
                $data[$input->name] = html_entity_decode($input->value);
            }
        }

        $buttons = $form->find('button');

        foreach ($buttons as $button) {
            $data[$button->name] = html_entity_decode($button->value);
        }

        foreach ($beforehandData as $key => $value) {
            $data[$key] = $value;
        }

        return [
            'action' => $action,
            'data' => $data,
        ];
    }
}