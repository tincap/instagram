<?php
/**
 * Created by PhpStorm.
 * User: tincap
 * Date: 23.10.17
 * Time: 23:10
 */

namespace app\models\helpers;


class ConsoleHelpers
{
    /**
     * Вывод в консоль сообщения
     *
     * @param $text
     * @param $color
     */
    public static function log($text, $color)
    {
        echo "\e[{$color}m" . $text . "\e[0m \n";
    }
}