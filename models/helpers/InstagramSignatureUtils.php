<?php

namespace app\models\helpers;

use InstagramAPI\Constants;

class InstagramSignatureUtils
{
    public static function generateUserAgent()
    {
        $deviceData = SignatureUtils::getDeviceData();

        return sprintf('Instagram %s Android (18/4.3; 320dpi; 720x1280; %s; %s; %s; qcom; en_US)', Constants::IG_VERSION, $deviceData[0], $deviceData[1], $deviceData[2]);
    }

    /**
     * Генерация user-agent для Web
     *
     * @return string
     */
    public static function generateWebUserAgent()
    {
        $userAgentList = [
            'Mozilla/5.0 (Windows; U; Windows NT 6.1; rv:2.2) Gecko/20110201',
            'Mozilla/5.0 (Windows; U; Windows NT 6.1; it; rv:2.0b4) Gecko/20100818',
            'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9a3pre) Gecko/20070330',
            'Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.9.2a1pre) Gecko',
            'Mozilla/5.0 (Windows; U; Windows NT 5.1; pl; rv:1.9.2.3) Gecko/20100401 Lightningquail/3.6.3',
            'Mozilla/5.0 (X11; U; SunOS5.10 sun4u; ja-JP; rv:1.5) Gecko/20031022',
            'Mozilla/5.0 (X11; U; Linux i686; de-AT; rv:1.5) Gecko/20031007',
            'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.6) Gecko/20040113',
            'Mozilla/5.0 (Windows; U; Windows NT 5.1; de-AT; rv:1.0.0) Gecko/20020530',
            'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36',
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.1 Safari/537.36',
            'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2226.0 Safari/537.36',
        ];

        return $userAgentList[array_rand($userAgentList)];
    }
}