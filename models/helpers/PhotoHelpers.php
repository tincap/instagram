<?php

namespace app\models\helpers;

use app\components\ImageResize;

class PhotoHelpers
{
    public static function small($photoPath, $size)
    {
        $resize = new ImageResize($photoPath);

        if ($resize->getDestWidth() > $size || $resize->getDestHeight() > $size) {

            if ($resize->getDestWidth() > $resize->getDestHeight()) {
                $resize->resizeToWidth($size);
                $resize->save($photoPath);
            } else {
                $resize->resizeToHeight($size);
                $resize->save($photoPath);
            }
        }

        $resize = null;
    }

    public static function horizontal($filename)
    {
        list($w_src, $h_src, $type) = getimagesize($filename);

        $types = array('', 'gif', 'jpeg', 'png');
        $ext = $types[$type];

        if ($ext) {
            $func = 'imagecreatefrom' . $ext;
            $src = $func($filename);
        }

        // создаём пустую квадратную картинку
        $dest = imagecreatetruecolor($w_src, $w_src);
        $white = imagecolorallocate($dest, 255, 255, 255);
        imagefill($dest, 0, 0, $white);

        // вырезаем квадратную верхушку по y,
        // если фото вертикальное (хотя можно тоже серединку)
        imagecopyresized($dest, $src, 0, ($h_src/2)/2, 0, 0, $w_src, $h_src, $w_src, $h_src);

        // вывод картинки и очистка памяти
        imagejpeg($dest, $filename);
    }

    // дополняет до квадрата
    public static function vertical($filename)
    {
        list($w_src, $h_src, $type) = getimagesize($filename);

        $types = array('', 'gif', 'jpeg', 'png');
        $ext = $types[$type];

        if ($ext) {
            $func = 'imagecreatefrom' . $ext;
            $src = $func($filename);
        }

        // создаём пустую квадратную картинку
        $dest = imagecreatetruecolor($h_src, $h_src);
        $white = imagecolorallocate($dest, 255, 255, 255);
        imagefill($dest, 0, 0, $white);

        imagecopyresized($dest, $src, (($h_src/2)/2)/2, 0, 0, 0, $w_src, $h_src, $w_src, $h_src);

        // вывод картинки и очистка памяти
        imagejpeg($dest, $filename);
    }
}