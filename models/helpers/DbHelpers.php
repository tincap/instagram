<?php

namespace app\models\helpers;

use app\models\App\User;

class DbHelpers
{
    public static function running()
    {
        $activeUser = User::getActiveUser();

        \Yii::$app->db->createCommand("UPDATE `account-" . $activeUser ."` SET id=(SELECT @a:=@a + 1 FROM (SELECT @a:= 0) as `account-" . $activeUser . "`);")->execute();
    }

    public static function mobileProxyRunning()
    {
        $activeUser = User::getActiveUser();

        \Yii::$app->db->createCommand("UPDATE `mobile-proxy-" . $activeUser ."` SET id=(SELECT @a:=@a + 1 FROM (SELECT @a:= 0) as `mobile-proxy-" . $activeUser . "`);")->execute();
    }
}