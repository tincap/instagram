<?php

namespace app\models\OldProject;

use yii\db\ActiveRecord;

class Account extends ActiveRecord
{
    /**
     * Email
     *
     * @return Proxy
     */
    public function getMail()
    {
        return static::hasOne(Mail::className(), ['id' => 'email_id']);
    }

    /**
     * Proxy
     *
     * @return Proxy
     */
    public function getProxy()
    {
        return static::hasOne(Proxy::className(), ['id' => 'proxy_id']);
    }

    /** @inheritdoc */
    public static function tableName()
    {
        return 'account-vulkan';
    }
}