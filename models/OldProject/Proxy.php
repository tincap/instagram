<?php

namespace app\models\OldProject;

use yii\db\ActiveRecord;

class Proxy extends ActiveRecord
{
    /**
     * Поиск по ip
     *
     * @param $ip
     * @return static
     */
    public static function findByIp($ip)
    {
        return static::findOne(['ip' => $ip]);
    }

    /**
     * Поиск по id
     *
     * @param $id
     * @return static
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /** @inheritdoc */
    public static function tableName()
    {
        return 'proxy-vulkan';
    }
}