<?php

namespace app\models\OldProject;

use yii\db\ActiveRecord;

class Mail extends ActiveRecord
{
    /**
     * Proxy
     *
     * @return Proxy
     */
    public function getProxy()
    {
        return static::hasOne(Proxy::className(), ['id' => 'proxy_id']);
    }

    /**
     * Поиск по username
     *
     * @param $username
     * @return static
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * Поиск по id
     *
     * @param $id
     * @return static
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /** @inheritdoc */
    public static function tableName()
    {
        return 'mail-account-vulkan';
    }
}