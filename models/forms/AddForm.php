<?php

namespace app\models\forms;

use app\models\helpers\DbHelpers;
use app\models\helpers\FileHelpers;
use app\models\Instagram\Proxy;
use app\models\App\User;
use yii\base\ErrorException;
use yii\base\Model;
use yii\db\Exception;

class AddForm extends Model
{
    public $accountList;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['accountList', 'filter', 'filter' => 'trim'],
            ['accountList', 'checkValidate'],
            ['accountList', 'required', 'message' => 'Пустое поле'],
            ['accountList', 'string', 'min' => 20, 'tooShort' => 'Слишком мало данных'],
        ];
    }

    /**
     * Проверка на корректность данных
     *
     * @param $attribute
     * @return bool
     */
    public function checkValidate($attribute)
    {
        $accountList = str_replace('|', ':', $this->accountList);
        $accountList = explode("\n", $accountList);

        $n = 0;

        foreach ($accountList as $item)
        {
            $n++;

            $item = trim($item);

            if (empty($item)) {
                continue;
            }

            $e = explode(':', $item);

            if (count($e) < 8) {
                $this->addError($attribute, "Неверный формат данных. Ошибка в строке №{$n}");
                return false;
            }
        }

        return true;
    }

    public function newList()
    {
        if ($this->validate()) {
            try {

                $activeUser = User::getActiveUser();

                \Yii::$app->db->createCommand()->truncateTable("account-" . $activeUser)->execute();
                \Yii::$app->db->createCommand()->truncateTable("mail-account-" . $activeUser)->execute();
                \Yii::$app->db->createCommand()->truncateTable("proxy-" . $activeUser)->execute();

                $accountList = str_replace('|', ':', $this->accountList);
                $accountList = explode("\n", $accountList);

                foreach ($accountList as $item) {
                    $item = trim($item);

                    if (empty($item)) {
                        continue;
                    }

                    $e = explode(':', $item);

                    if (isset($e[8])) {
                        $author = $e[8];
                    } else {
                        $author = 'rinat';
                    }

                    \Yii::$app->db->createCommand("INSERT INTO `account-" . $activeUser . "` SET username = '" . $e[0] . "', password = '" . $e[1] . "', author = '" . $author . "'")->execute();
                    \Yii::$app->db->createCommand("INSERT INTO `proxy-" . $activeUser . "` SET ip = '" . $e[2] . ":" . $e[3] . "', password = '" . $e[4] . ":" . $e[5] . "'")->execute();
                    \Yii::$app->db->createCommand("INSERT INTO `mail-account-" . $activeUser . "` SET username = '" . $e[6] . "', password = '" . $e[7] . "'")->execute();
                }

                // Настраиваем аккаунты
                \Yii::$app->db->createCommand("UPDATE `account-" . $activeUser . "` SET proxy_id = id, email_id = id")->execute();

                // Настраиваем прокси мыл
                for ($i = 0; $i < 15; $i++) {
                    \Yii::$app->db->createCommand("UPDATE `mail-account-" . $activeUser . "` SET proxy_id = id - 400 * $i WHERE id > 400 * $i")->execute();
                }

                FileHelpers::deleteDirectory(\Yii::getAlias('@app/data/Mail/cookies/' . $activeUser));
                mkdir(\Yii::getAlias('@app/data/Mail/cookies/' . $activeUser), 0777);

                FileHelpers::deleteDirectory(\Yii::getAlias("@vendor/mgp25/instagram-php/sessions"));
                mkdir(\Yii::getAlias("@vendor/mgp25/instagram-php/sessions", 0777));

                return true;

            } catch (Exception $e) {
                $this->addError('accountList', "Произошла системная в базе данных. " . $e->getMessage());
            } catch (ErrorException $e) {
                $this->addError('accountList', "Произошла системная в коде. " . $e->getMessage());
            }
        }

        return false;
    }

    public function add()
    {
        if ($this->validate()) {

            try {
                $activeUser = User::getActiveUser();

                $accountList = str_replace('|', ':', $this->accountList);
                $accountList = explode("\n", $accountList);

                $sqlSelect = Proxy::find()->select("id")->orderBy('id DESC')->asArray()->one();
                $maxIdProxy = $sqlSelect['id'];

                $sqlSelect = \app\models\Mail\Account::find()->select("id")->orderBy('id DESC')->asArray()->one();
                $maxIdMail = $sqlSelect['id'];

                foreach ($accountList as $item) {

                    $maxIdProxy++;
                    $maxIdMail++;

                    $item = trim($item);

                    if (empty($item)) {
                        continue;
                    }

                    $e = explode(':', $item);

                    if (isset($e[8])) {
                        $author = $e[8];
                    } else {
                        $author = 'rinat';
                    }

                    \Yii::$app->db->createCommand("INSERT INTO `account-" . $activeUser . "` SET username = '" . $e[0] . "', password = '" . $e[1] . "', author = '" . $author . "'" . ", proxy_id = $maxIdProxy, email_id = $maxIdMail")->execute();
                    \Yii::$app->db->createCommand("INSERT INTO `proxy-" . $activeUser . "` SET ip = '" . $e[2] . ":" . $e[3] . "', password = '" . $e[4] . ":" . $e[5] . "'")->execute();
                    \Yii::$app->db->createCommand("INSERT INTO `mail-account-" . $activeUser . "` SET username = '" . $e[6] . "', password = '" . $e[7] . "'")->execute();
                }

                // Настраиваем прокси мыл
                for ($i = 0; $i < 15; $i++) {
                    \Yii::$app->db->createCommand("UPDATE `mail-account-" . $activeUser . "` SET proxy_id = id - 400 * $i WHERE id > 400 * $i")->execute();
                }

                FileHelpers::deleteDirectory(\Yii::getAlias('@app/data/Mail/cookies/' . $activeUser));
                mkdir(\Yii::getAlias('@app/data/Mail/cookies/' . $activeUser), 0777);

                DbHelpers::running();

                return true;

            } catch (Exception $e) {
                $this->addError('accountList', "Произошла системная в базе данных. " . $e->getMessage());
            } catch (ErrorException $e) {
                $this->addError('accountList', "Произошла системная в коде. " . $e->getMessage());
            }
        }

        return false;
    }
}