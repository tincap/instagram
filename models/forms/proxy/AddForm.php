<?php

namespace app\models\forms\proxy;

use app\models\App\User;
use app\models\helpers\DbHelpers;
use app\models\Instagram\MobileProxy;
use Exception;
use yii\base\Model;

class AddForm extends Model
{
    public $proxyList;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['proxyList', 'filter', 'filter' => 'trim'],
            ['proxyList', 'required', 'message' => 'Пустое поле'],
            ['proxyList', 'checkValidate'],
            ['proxyList', 'string', 'min' => 20, 'tooShort' => 'Слишком мало данных'],
        ];
    }



    public function checkValidate($attribute)
    {
        $proxyList = explode("\n", $this->proxyList);

        $n = 1;

        foreach ($proxyList as $proxy) {

            $e = explode("|", $proxy);

            if (count($e) < 3) {
                $this->addError($attribute, "Неверный формат в строке #{$n}");
                return false;
            }

            $n++;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function add()
    {
        if ($this->validate()) {

            try {

                \Yii::$app->db->createCommand()->truncateTable("mobile-proxy-" . User::getActiveUser())->execute();

                $proxyList = explode("\n", $this->proxyList);

                foreach ($proxyList as $proxy) {

                    $e = explode("|", $proxy);

                    $mobileProxy = new MobileProxy();
                    $mobileProxy->ip = $e[0];
                    $mobileProxy->password = $e[1];
                    $mobileProxy->update_url = $e[2];
                    $mobileProxy->save();
                }

                DbHelpers::mobileProxyRunning();

                return true;

            } catch (Exception $e) {
                $this->addError('proxyList', "Произошла системная в базе данных. " . $e->getMessage());
            }
        }

        return false;
    }
}