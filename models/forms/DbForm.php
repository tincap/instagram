<?php

namespace app\models\forms;

class DbForm
{
    CONST DELETE_UNWORKING = 'delete_unworking_accounts';
    CONST DELETE_BANNED = 'delete_banned_accounts';

    CONST UPDATE_PHOTO = 'update_photo';
}