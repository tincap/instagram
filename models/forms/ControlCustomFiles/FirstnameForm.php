<?php

namespace app\models\forms\ControlCustomFiles;

use app\models\helpers\FileHelpers;
use yii\base\Model;

class FirstnameForm extends Model
{
    public $firstnameList;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['firstnameList', 'filter', 'filter' => 'trim'],
            ['firstnameList', 'required', 'message' => 'Пустое поле'],
            ['firstnameList', 'string', 'min' => 20, 'tooShort' => 'Слишком мало данных'],
        ];
    }

    /**
     * Добавляем новые элементы к старым
     *
     * @return bool
     */
    public function add()
    {
        if ($this->validate()) {
            $items = explode("\n", $this->firstnameList);
            $correctItemList = [];

            foreach ($items as $item) {
                if (strlen($item) > 30 || strlen($item) < 3) {
                    continue;
                }

                $correctItemList[] = $item;
            }

            $firstnamePath = \Yii::getAlias('@app/data/files/custom/firstnameList.txt');
            $text = file_get_contents($firstnamePath);
            $items = explode("\n", $text);

            foreach ($items as $item) {
                if (strlen($item) > 30 || strlen($item) < 3) {
                    continue;
                }

                $correctItemList[] = $item;
            }

            $text = implode("\n", $correctItemList);

            $fp = fopen($firstnamePath, 'w');
            $test = fwrite($fp, $text);

            if ($test) {
                fclose($fp);
                return true;
            }

            $this->addError('firstnameList', 'Не смогли записать элементы в файл firstnameList.txt');
        }

        return false;
    }

    /**
     * Передобавляем список в файл. Прошлые элементы уничтожаются
     *
     * @return bool
     */
    public function anew()
    {
        if ($this->validate()) {
            $items = explode("\n", $this->firstnameList);
            $correctItemList = [];

            foreach ($items as $item) {
                if (strlen($item) > 30 || strlen($item) < 3) {
                    continue;
                }

                $correctItemList[] = $item;
            }

            $firstnamePath = \Yii::getAlias('@app/data/files/custom/firstnameList.txt');

            $text = implode("\n", $correctItemList);

            $fp = fopen($firstnamePath, 'w');
            $test = fwrite($fp, $text);

            if ($test) {
                fclose($fp);
                return true;
            }

            $this->addError('firstnameList', 'Не смогли записать элементы в файл firstnameList');
        }

        return false;
    }
}