<?php

namespace app\models\forms\ControlCustomFiles;

use app\models\helpers\FileHelpers;
use yii\base\Model;

class BiographyForm extends Model
{
    public $biographyList;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['biographyList', 'filter', 'filter' => 'trim'],
            ['biographyList', 'required', 'message' => 'Пустое поле'],
            ['biographyList', 'string', 'min' => 20, 'tooShort' => 'Слишком мало данных'],
        ];
    }

    /**
     * Добавляем новые элементы к старым
     *
     * @return bool
     */
    public function add()
    {
        if ($this->validate()) {
            $biographyPath = \Yii::getAlias('@app/data/files/custom/biographyList.txt');
            $fp = fopen($biographyPath, 'w');
            $test = fwrite($fp, $this->biographyList);

            if ($test) {
                fclose($fp);
                return true;
            }

            $this->addError('biographyList', 'Не смогли записать элементы в файл biographyList.txt');
        }

        return false;
    }
}