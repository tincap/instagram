<?php

namespace app\models\forms\ControlCustomFiles;

use app\models\helpers\FileHelpers;
use yii\base\Model;

class PhotoCaptionsForm extends Model
{
    public $photoCaptions;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['photoCaptions', 'filter', 'filter' => 'trim'],
            ['photoCaptions', 'required', 'message' => 'Пустое поле'],
            ['photoCaptions', 'string', 'min' => 20, 'tooShort' => 'Слишком мало данных'],
        ];
    }

    /**
     * Добавляем новые элементы к старым
     *
     * @return bool
     */
    public function add()
    {
        if ($this->validate()) {
            $biographyPath = \Yii::getAlias('@app/data/files/custom/photoCaptions.txt');

            $fp = fopen($biographyPath, 'w');
            $test = fwrite($fp, $this->photoCaptions);

            if ($test) {
                fclose($fp);
                return true;
            }

            $this->addError('photoCaptions', 'Не смогли записать элементы в файл photoCaptions.txt');
        }

        return false;
    }
}