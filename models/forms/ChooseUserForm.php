<?php

namespace app\models\forms;

use app\models\App\User;
use yii\base\Exception;
use yii\base\Model;

class ChooseUserForm extends Model
{
    public $user;

    public $newUser;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['user', 'filter', 'filter' => 'trim'],
            ['newUser', 'filter', 'filter' => 'trim'],
            ['newUser', 'required', 'message' => 'Введите имя базы'],
            ['newUser', 'string', 'min' => 3, 'max' => 20, 'tooShort' => 'Слишком короткое имя', 'tooLong' => 'Слишком длинное имя'],
            ['newUser', 'checkUnique'],
        ];
    }

    /**
     * Проверка на уникальность
     *
     * @param $attribute
     * @return bool
     */
    public function checkUnique($attribute)
    {
        if (!$this->hasErrors()) {
            if (User::inDb($this->newUser)) {
                $this->addError($attribute, 'Такой пользователь уже есть в базе данных');
                return false;
            }
        }

        return true;
    }

    public function chooseUser()
    {
        try {
            User::chooseUser($this->user);
            return true;
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function addNewUser()
    {
        if ($this->validate()) {

            try {
                User::addNewUser($this->newUser);
                User::chooseUser($this->newUser);

                return true;
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        return false;
    }

    public function action()
    {
        if (!empty($this->newUser)) {
            return $this->addNewUser();
        } elseif (!empty($this->user)) {
            return $this->chooseUser();
        }

        return false;
    }
}