<?php

namespace app\controllers;

use app\models\forms\AddForm;
use yii\web\Controller;

class AddController extends Controller
{
    public function actionStandard()
    {
        $model = new AddForm();

        if ($model->load(\Yii::$app->request->post())) {

            if (\Yii::$app->request->post('add') == 1 && $model->add()) {
                return $this->goHome();
            }

            if (\Yii::$app->request->post('add') == 2 && $model->newList()) {
                return $this->goHome();
            }


        }

        return $this->render('add-new-list', [
            'model' => $model,
        ]);
    }
}