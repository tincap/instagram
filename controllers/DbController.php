<?php

namespace app\controllers;

use app\models\App\User;
use app\models\forms\DbForm;
use app\models\helpers\DbHelpers;
use yii\web\Controller;

class DbController extends Controller
{
    public function actionSettings()
    {
        return $this->render('settings');
    }

    public function actionGo($actionType)
    {
        switch ($actionType) {
            case DbForm::DELETE_UNWORKING:

                \Yii::$app->db->createCommand()->delete("account-" . User::getActiveUser(), 'working = 0')->execute();
                DbHelpers::running();
                break;

            case DbForm::DELETE_BANNED:

                \Yii::$app->db->createCommand()->delete("account-" . User::getActiveUser(), "banned = 1 OR error_type = 'incorrect_password'")->execute();
                DbHelpers::running();
                break;

            case DbForm::UPDATE_PHOTO:

                \Yii::$app->db->createCommand()->update("account-" . User::getActiveUser(), ['photo' => 1])->execute();
                break;
        }

        $this->goHome();
    }
}