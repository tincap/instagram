<?php

namespace app\controllers;

use app\models\forms\proxy\AddForm;
use app\models\Instagram\MobileProxy;
use yii\web\Controller;

class ProxyController extends Controller
{
    public function actionList()
    {
        $model = new AddForm();

        if ($model->load(\Yii::$app->request->post())) {
            $model->add();
        }

        $mobileProxyList = MobileProxy::find()->all();

        return $this->render('list', [
            'model' => $model,
            'mobileProxyList' => $mobileProxyList,
            'errors' => $model->errors,
        ]);
    }
}