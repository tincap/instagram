<?php

namespace app\controllers;

use app\models\Instagram\Account;
use yii\web\Controller;

class AccountListController extends Controller
{
    /**
     * Standard
     */
    public function actionStandard()
    {
        $accountList = Account::find()->where('')->all();

        foreach ($accountList as $account) {

            /** @var Account $account */
            echo $account->username . ':' . $account->password . '|' . $account->proxy->ip . ':' . $account->proxy->password . ':' . $account->mail->username . ':' . $account->mail->password . "<br>";
        }
    }

    /**
     * Zaliv
     */
    public function actionZaliv()
    {
        $accountList = Account::find()->where('')->all();

        foreach ($accountList as $account) {

            /** @var Account $account */
            echo ($account->zaliv_name == null ? 'NULL' : $account->zaliv_name) . '|' . $account->username . ':' . $account->password . '|' . $account->proxy->ip . ':' . $account->proxy->password . '|' . $account->mail->username . ':' . $account->mail->password . "<br>";
        }
    }

    /**
     * Filled
     */
    public function actionFilled()
    {
        $accountList = Account::find()->where('photo = 1 and filled = 1 and biographed = 1 and avatar = 1')->all();

        foreach ($accountList as $account) {

            /** @var Account $account */
            echo $account->username . ':' . $account->password . '|' . $account->proxy->ip . ':' . $account->proxy->password . ':' . $account->mail->username . ':' . $account->mail->password . "<br>";
        }
    }

    /**
     * SocialKit
     */
    public function actionSocialkit()
    {
        $accountList = Account::find()->where('')->all();

        foreach ($accountList as $account) {

            /** @var Account $account */
            echo $account->username . ';' . $account->password . ';' . $account->proxy->ip . ';' . str_replace(':', ';', $account->proxy->password) . ';1' . '<br>';
        }
    }

    /**
     * Instap
     */
    public function actionInstap()
    {
        $accountList = Account::find()->all();

        foreach ($accountList as $account) {

            /** @var Account $account */
            echo $account->username . ':' . $account->password . '|' . $account->proxy->ip . ':' . $account->proxy->password . '<br>';
        }
    }

    /**
     * Standard
     */
    public function actionSms()
    {
        $accountList = Account::find()->where('working = 0 and (error_type = "sms" or error_type = "sms_add_phone" or error_type = "sms_go_back")')->all();

        foreach ($accountList as $account) {

            /** @var Account $account */
            echo $account->username . ':' . $account->password . '|' . $account->proxy->ip . ':' . $account->proxy->password . ':' . $account->author . "<br>";
        }
    }

    /**
     * Standard
     */
    public function actionEmail()
    {
        $accountList = Account::find()->where('working = 0 and error_type = "email"')->all();

        foreach ($accountList as $account) {

            /** @var Account $account */
            echo $account->username . ':' . $account->password . '|' . $account->proxy->ip . ':' . $account->proxy->password . ':' . $account->mail->username . ':' . $account->mail->password . "<br>";
        }
    }

    /**
     * Instap
     *
     * Список аккаунтов, у которых нет описания профиля
     */
    public function actionBiography()
    {
        $accountList = Account::find()->where('biographed = 0')->all();

        foreach ($accountList as $account) {

            /** @var Account $account */
            echo $account->username . ':' . $account->password . '|' . $account->proxy->ip . ':' . $account->proxy->password . ':' . $account->mail->username . ':' . $account->mail->password . "<br>";
        }
    }

    /**
     * banned
     */
    public function actionBanned()
    {
        $accountList = Account::find()->where("banned = 1 OR error_type = 'incorrect_password'")->all();

        foreach ($accountList as $account) {
            /** @var Account $account */
            echo $account->proxy->ip . ':' . $account->proxy->password . "<br>";
        }
    }

    /**
     * unknown
     */
    public function actionUnknown()
    {
        $accountList = Account::find()->where("working = 0 && (error_type IS NULL or error_type = 'unknown')")->all();

        foreach ($accountList as $account) {
            /** @var Account $account */
            echo $account->proxy->ip . ':' . $account->proxy->password . "<br>";
        }
    }

    /**
     * Тесты
     */
    public function actionTrash()
    {

    }
}