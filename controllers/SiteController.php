<?php

namespace app\controllers;

use app\models\Instagram\Account;
use yii\web\Controller;

class SiteController extends Controller
{
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $accountList = Account::find()->all();
        return $this->render('index', [
            'accountList' => $accountList,
        ]);
    }
}