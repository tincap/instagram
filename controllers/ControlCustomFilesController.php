<?php

namespace app\controllers;

use app\models\forms\ControlCustomFiles\BiographyForm;
use app\models\forms\ControlCustomFiles\FirstnameForm;
use app\models\forms\ControlCustomFiles\PhotoCaptionsForm;
use app\models\helpers\FileHelpers;
use yii\web\Controller;

class ControlCustomFilesController extends Controller
{
    public function actionFileList()
    {
        return $this->render('file-list');
    }

    public function actionFirstname()
    {
        $model = new FirstnameForm();

        if ($model->load(\Yii::$app->request->post())) {
            $model->add();
        }

        $filePath = \Yii::getAlias("@app/data/files/custom/firstnameList.txt");
        $text = file_get_contents($filePath);
        $items = explode("\n", $text);

        $items = array_reverse($items);

        return $this->render('firstname', [
            'items' => $items,
            'model' => $model,
        ]);
    }

    public function actionBiography()
    {
        $model = new BiographyForm();

        if ($model->load(\Yii::$app->request->post())) {
            $model->add();
        }

        $filePath = \Yii::getAlias("@app/data/files/custom/biographyList.txt");

        $text = file_get_contents($filePath);

        $items = explode(FileHelpers::BORDER, $text);

        $items = array_reverse($items);

        return $this->render('biography', [
            'items' => $items,
            'model' => $model,
        ]);
    }

    public function actionPhotoCaptions()
    {
        $model = new PhotoCaptionsForm();

        if ($model->load(\Yii::$app->request->post())) {
            $model->add();
        }

        $filePath = \Yii::getAlias("@app/data/files/custom/photoCaptions.txt");

        $text = file_get_contents($filePath);

        $items = explode(FileHelpers::BORDER, $text);

        $items = array_reverse($items);

        return $this->render('photo-captions', [
            'items' => $items,
            'model' => $model,
        ]);
    }
}