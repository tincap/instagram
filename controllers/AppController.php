<?php

namespace app\controllers;

use app\models\forms\ChooseUserForm;
use app\models\helpers\FileDbHelpers;
use app\models\App\User;
use yii\web\Controller;

class AppController extends Controller
{
    public function actionChooseUser()
    {
        $db = FileDbHelpers::read(User::getUserlistFilename());

        $userlist = [];

        foreach ($db as $key => $value) {
            $userlist[$value] = $value;
        }

        $model = new ChooseUserForm();

        if ($model->load(\Yii::$app->request->post()) && $model->action()) {
            return $this->goHome();
        } else {
            return $this->render('choose-user', [
                'userlist' => $userlist,
                'model' => $model,
                'errors' => $model->errors,
            ]);
        }
    }
}