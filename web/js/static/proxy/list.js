$(document).ready(function() {

    var countProxy = $('.id').length;
    var n = 0;

    $('.id').each(function () {

        var id = $(this).attr("id");

        $.ajax({
            type: "GET",
            url: "/ajax/mobile-proxy/discover-ip/" + id,
            dataType: 'json',
            success: function (response) {
                if (response.status == 'ok') {
                    $('#mobile_proxy_' + id).html(response.ip);
                }

                if (response.status == 'fail') {
                    $('#mobile_proxy_' + id).addClass('danger');
                    $('#mobile_proxy_' + id).html(response.message);
                }

                n++;

                if (n >= countProxy) {
                    $("#update_proxy").show();
                }
            }
        });
    });

    $("#update_proxy").click(function () {

        $("#update_proxy").toggleClass('active');

        $('.id').each(function () {

            var id = $(this).attr("id");

            $('#mobile_proxy_' + id).removeClass("danger");
            $('#mobile_proxy_' + id).html("Обновление...");

            $.ajax({
                type: "GET",
                url: "/ajax/mobile-proxy/update/" + id,
                dataType: 'json',
                success: function (response) {
                    if (response.status == 'ok') {
                        $('#mobile_proxy_' + id).addClass('success');
                        $('#mobile_proxy_' + id).html(response.ip);
                    }

                    if (response.status == 'fail') {
                        $('#mobile_proxy_' + id).addClass('danger');
                        $('#mobile_proxy_' + id).html(response.message);
                    }
                }
            });
        });

    })

});