<?php

namespace app\modules\ajax;

use Yii;
use yii\web\Response;

class Module extends \yii\base\Module
{
    public function init()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $this->layout = false;

        parent::init();
    }
}