<?php

namespace app\modules\ajax\controllers;

use app\models\Instagram\Exceptions\MobileProxyException;
use app\models\Instagram\MobileProxy;

class MobileProxyController extends Controller
{
    /**
     * Обновить прокси
     *
     * @param $mobileProxyId
     * @return array
     */
    public function actionUpdate($mobileProxyId)
    {
        $mobileProxy = MobileProxy::findIdentity($mobileProxyId);

        try {
            $mobileProxy->updateProxy();

            return [
                'status' => 'ok',
                'ip' => $mobileProxy->discoverIp(),
            ];
        } catch (MobileProxyException $e) {
            return [
                'status' => 'fail',
                'message' => $e->getMessage(),
            ];
        }
    }

    /**
     * Обновить прокси
     *
     * @param $mobileProxyId
     * @return array
     */
    public function actionDiscoverIp($mobileProxyId)
    {
        $mobileProxy = MobileProxy::findIdentity($mobileProxyId);

        try {

            $ip = $mobileProxy->discoverIp();

            return [
                'status' => 'ok',
                'ip' => $ip,
            ];

        } catch (MobileProxyException $e) {
            return [
                'status' => 'fail',
                'message' => $e->getMessage(),
            ];
        }
    }
}