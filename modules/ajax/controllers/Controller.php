<?php

namespace app\modules\ajax\controllers;

class Controller extends \yii\web\Controller
{
    public $enableCsrfValidation = false;
}