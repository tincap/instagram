<?php

namespace app\modules\api\controllers;

use app\components\ImageResize;
use app\models\helpers\FileHelpers;
use app\models\helpers\PhotoHelpers;
use app\models\Instagram\Account;
use InstagramAPI\Exception\InstagramException;

class MediaController extends Controller
{
    /**
     * Возвращает список media аккаунта
     *
     * @param $accountId
     * @return array
     */
    public function actionGetList($accountId)
    {
        $account = Account::findIdentity($accountId);

        if ($account->banned || !$account->working) {
            return [
                'status' => 'fail',
                'message' => 'Аккаунт не работает',
            ];
        }

        try {
            $account->prepare(false);
            $account->login();

            $response = $account->instagram->timeline->getSelfUserFeed();

            $mediaList = [];

            foreach ($response->getItems() as $item) {
                $mediaList[] = $item->getId();
            }

            return [
                'status' => 'ok',
                'mediaIdList' => $mediaList,
            ];

        } catch (InstagramException $e) {
            return $this->catchErrors($e, $account);
        }
    }

    public function actionGetCommentList($accountId, $mediaId)
    {
        $account = Account::findIdentity($accountId);

        if ($account->banned || !$account->working) {
            return [
                'status' => 'fail',
                'message' => 'Аккаунт не работает',
            ];
        }

        try {

            $account->prepare(false);
            $account->login();

            $response = $account->instagram->media->getComments($mediaId);

            $commentList = [];

            foreach ($response->getComments() as $comment) {
                $commentList[] = $comment->getPk();
            }

            return [
                'status' => 'ok',
                'commentList' => $commentList,
            ];

        } catch (InstagramException $e) {
            return $this->catchErrors($e, $account);
        }
    }

    public function actionDeleteComment($accountId, $mediaId, $commentId)
    {
        $account = Account::findIdentity($accountId);

        if ($account->banned || !$account->working) {
            return [
                'status' => 'fail',
                'message' => 'Аккаунт не работает',
            ];
        }

        $account->prepare(false);
        $account->login();

        try {
            $account->instagram->media->deleteComment($mediaId, $commentId);

            return [
                'status' => 'ok',
            ];

        } catch (InstagramException $e) {
            return $this->catchErrors($e, $account);
        }
    }

    public function actionDeleteLastPhotos($accountId)
    {
        $account = Account::findIdentity($accountId);

        if ($account->banned || !$account->working) {
            return [
                'status' => 'fail',
                'message' => 'Аккаунт не работает',
            ];
        }

        try {
            $account->prepare(false);
            $account->login();

            $mediaCount = $account->getMediaCount();

            if ($mediaCount > 6 && $mediaCount < 18) {

                $response = $account->instagram->timeline->getSelfUserFeed();

                $n = 1;

                $mediaList = array_reverse($response->getItems());

                foreach ($mediaList as $item) {

                    if ($n <= 6) {
                        $n++;
                        continue;
                    }

                    $account->instagram->media->delete($item->getId());

                    $n++;

                    sleep(5);
                }

                $mediaCount = $account->getMediaCount();
                $account->media_count = $mediaCount;
                $account->save();

                return [
                    'status' => 'ok',
                    'delete' => true,
                ];
            }

            return [
                'status' => 'ok',
                'delete' => false,
            ];

        } catch (InstagramException $e) {
            return $this->catchErrors($e, $account);
        }
    }

    /**
     * Удалить media аккаунта
     *
     * @param $accountId
     * @param $mediaId
     * @return array
     */
    public function actionDelete($accountId, $mediaId)
    {
        $account = Account::findIdentity($accountId);

        if ($account->banned || !$account->working) {
            return [
                'status' => 'fail',
                'message' => 'Аккаунт не работает',
            ];
        }

        try {
            $account->prepare(false);
            $account->login();
            $account->instagram->media->delete($mediaId);

            return [
                'status' => 'ok',
            ];

        } catch (InstagramException $e) {
            return [
                'status' => 'fail',
                'message' => $e->getMessage(),
            ];
        }
    }

    public function actionUploadPhoto($accountId, $photoId)
    {
        $account = Account::findIdentity($accountId);

        if ($account->banned || !$account->working) {
            return [
                'status' => 'fail',
                'message' => 'Аккаунт не работает',
            ];
        }

        if ($account->follower_count == null || $account->follower_count < 100) {
            return [
                'status' => 'fail',
                'message' => 'Подписчиков меньше чем 100',
            ];
        }

        $photoPath = \Yii::getAlias("@app/data/Instagram/upload/{$photoId}.jpg");

        if (!file_exists($photoPath)) {
            $photoPath = \Yii::getAlias("@app/data/Instagram/upload/{$photoId}.JPG");
        }

        if (!file_exists($photoPath)) {
            return [
                'status' => 'fail',
                'message' => 'Не нашли нужную фотографию',
            ];
        }

        if ($account->banned || !$account->working) {
            return [
                'status' => 'fail',
                'message' => 'Аккаунт не работает',
            ];
        }

        try {
            $account->prepare(false);
            $account->login();

            if ($account->getMediaCount() > 6) {
                return [
                    'status' => 'fail',
                    'message' => 'Фотографий больше чем 6',
                ];
            }

            sleep(1);

            $userFeed = $account->instagram->timeline->getSelfUserFeed();

            $caption = null;

            foreach ($userFeed->getItems() as $item) {
                if (strlen($item->getCaption()->getText()) > 20) {
                    $caption = $item->getCaption()->getText();
                    break;
                }
            }

            if ($caption == null) {
                $account->filled = 0;
                $account->save();

                return [
                    'status' => 'fail',
                    'message' => 'Фотографии без описания',
                ];
            }

            $imageResize = new ImageResize($photoPath);

            if ($imageResize->getDestWidth() > \Yii::$app->params['photo_size'] || $imageResize->getDestHeight() > \Yii::$app->params['photo_size']) {

                if ($imageResize->getDestWidth() > $imageResize->getDestHeight()) {
                    $imageResize->resizeToWidth(\Yii::$app->params['photo_size']);
                    $imageResize->save($photoPath);
                } else {
                    $imageResize->resizeToHeight(\Yii::$app->params['photo_size']);
                    $imageResize->save($photoPath);
                }
            }

            if ($imageResize->getDestWidth() > $imageResize->getDestHeight()) {
                PhotoHelpers::horizontal($photoPath);
            } else {
                if ($imageResize->getDestWidth() < $imageResize->getDestHeight()) {
                    PhotoHelpers::vertical($photoPath);
                }
            }

            $imageResize = null;

            $result = $account->instagram->timeline->uploadPhoto($photoPath);

            sleep(rand(2, 4));

            $account->instagram->media->edit($result->getMedia()->getId(), $caption);

            $account->media_count++;
            $account->save();

            return [
                'status' => 'ok',
            ];

        } catch (InstagramException $e) {
            return [
                'status' => 'fail',
                'message' => $e->getMessage(),
            ];
        }
    }

    public function actionCountPhotosForUpload()
    {
        $directory = \Yii::getAlias("@app/data/Instagram/upload/");

        if (!is_dir($directory)) {
            return [
                'status' => 'fail',
                'message'=> 'Не нашли папку upload',
            ];
        }

        $count = FileHelpers::countFile($directory);

        return [
            'status' => 'ok',
            'count' => $count,
        ];
    }
}