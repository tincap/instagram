<?php

namespace app\modules\api\controllers;

use app\models\Instagram\Account;
use app\models\Instagram\checkpoint\CheckpointCaptcha;
use app\models\Instagram\checkpoint\Discover;
use app\models\Instagram\checkpoint\Exceptions\CheckpointException;
use app\models\Instagram\Exceptions\AccountModelException;
use InstagramAPI\Exception\InstagramException;

class AccountController extends Controller
{
    /**
     * Check
     *
     * @param $accountId
     * @return array
     */
    public function actionCheck($accountId)
    {
        $account = Account::findIdentity($accountId);

        try {
            $account->prepare(false);

            if ($account->isLoggedIn()) {
                $account->instagram->isMaybeLoggedIn = true;
            } else {
                $account->deleteCookieFile();
                $account->instagram->isMaybeLoggedIn = false;
            }

            $account->instagram->login($account->username, $account->password);

            $account->working = 1;
            $account->error_type = null;
            $account->save();

            return [
                'status' => 'ok',
            ];

        } catch (InstagramException $e) {
            return $this->catchErrors($e, $account);
        }
    }

    /**
     * Подписаться
     *
     * @param $accountId
     * @param $userId
     * @param $mobileProxyId
     * @return array
     */
    public function actionFollow($accountId, $userId, $mobileProxyId)
    {
        $account = Account::findIdentity($accountId);
        $account->prepare($mobileProxyId);

        try {
            $account->login();

            $userInfo = $account->instagram->people->getInfoById($userId);

            if ($userInfo->hasUser()) {
                if ($userInfo->getUser()->getIsPrivate()) {
                    return [
                        'status' => 'fail',
                        'stop_follow' => false,
                        'stop_iteration' => false,
                        'skip' => true,
                        'message' => 'Пользователь приватный',
                    ];
                }
            } else {
                return [
                    'status' => 'fail',
                    'stop_follow' => false,
                    'stop_iteration' => false,
                    'skip' => false,
                    'message' => 'Не смогли получить информацию о пользователе',
                ];
            }

            $account->instagram->people->follow($userId);

            return [
                'status' => 'ok',
            ];

        } catch (InstagramException $e) {

            // Feedback
            if (preg_match('/Feedback required/i', $e->getMessage())) {
                $account->error_type = Discover::ERROR_FEEDBACK;
                $account->working = 0;
                $account->save();

                return [
                    'status' => 'fail',
                    'stop_follow' => true,
                    'stop_iteration' => true,
                    'skip' => false,
                    'message' => $e->getMessage(),
                ];
            }

            // СЛИШКОМ МНОГО ЗАПРОСОВ
            if (preg_match('/Throttled by Instagram because of too many API requests/i', $e->getMessage())) {
                return [
                    'status' => 'fail',
                    'stop_follow' => false,
                    'stop_iteration' => true,
                    'skip' => false,
                    'message' => $e->getMessage(),
                ];
            }

            $discover = new Discover($account);
            $errorType = $discover->identityCheckpointTypeByResponse($e->getResponse());

            // КАПЧА
            if ($errorType == Discover::ERROR_CAPTCHA) {

                if ($errorType == Discover::ERROR_CAPTCHA) {
                    $checkpoint = new CheckpointCaptcha($account, $discover->getHTML(), $discover->getCheckpointUrl(), false);

                    try {
                        $checkpoint->doCheckpoint();

                        return [
                            'status' => 'fail',
                            'stop_follow' => false,
                            'stop_iteration' => true,
                            'skip' => false,
                            'message' => "Смогли починить капчу",
                        ];

                    } catch (CheckpointException $e) {

                        $account->error_type = $errorType;
                        $account->working = 0;
                        $account->save();

                        return [
                            'status' => 'fail',
                            'stop_follow' => true,
                            'stop_iteration' => true,
                            'skip' => false,
                            'message' => "Вышла капча. Не смогли починить. " . $e->getMessage(),
                        ];
                    }
                }
            }

            // СМС
            if ($errorType == Discover::ERROR_SMS || $errorType == Discover::ERROR_SMS_ADD_PHONE || $errorType == Discover::ERROR_SMS_GO_BACK) {
                $account->error_type = $errorType;
                $account->working = 0;
                $account->save();

                return [
                    'status' => 'fail',
                    'stop_follow' => true,
                    'stop_iteration' => true,
                    'skip' => false,
                    'message' => $e->getMessage(),
                ];
            }

            // БАН
            if ($errorType == Discover::ERROR_DISABLED) {
                $account->working = 0;
                $account->banned = 1;
                $account->save();
                return [
                    'status' => 'fail',
                    'stop_follow' => true,
                    'stop_iteration' => true,
                    'skip' => false,
                    'message' => $e->getMessage(),
                ];
            }

            return [
                'status' => 'fail',
                'stop_follow' => false,
                'stop_iteration' => false,
                'message' => $e->getMessage(),
            ];

        } catch (AccountModelException $e) {
            return [
                'status' => 'fail',
                'stop_follow' => false,
                'stop_iteration' => false,
                'skip' => false,
                'message' => $e->getMessage(),
            ];
        }
    }

    /**
     * Авторизация
     *
     * @param $accountId
     * @param $mobileProxy
     * @param $forceLogin
     * @return array
     */
    public function actionLogin($accountId, $mobileProxy, $forceLogin)
    {
        $account = Account::findIdentity($accountId);
        $account->prepare($mobileProxy);

        try {
            $account->login($forceLogin);

            // Успешно
            $account->working = 1;
            $account->error_type = null;
            $account->save();

            return [
                'status' => 'ok',
            ];
        } catch (InstagramException $e) {

            $discover = new Discover($account);
            $account->error_type = $discover->identityCheckpointTypeByResponse($e->getResponse());

            if ($discover->isBanned()) {
                $account->banned = 1;
            }

            $account->working = 0;
            $account->save();

            $message = $account->translateErrorType();

            return [
                'status' => 'fail',
                'message' => $message == 'unknown' ? $e->getMessage() : $message,
            ];
        }
    }

    /**
     * Количество аккаунтов
     *
     * @return array
     */
    public function actionCount()
    {
        return [
            'status' => 'ok',
            'count' => Account::find()->count('id'),
        ];
    }

    /**
     * Возвращает файл со списком подписок аккаунта
     *
     * @param $accountId
     * @return array
     */
    public function actionGetAccountFollowListFile($accountId)
    {
        $account = Account::findIdentity($accountId);

        return [
            'status' => 'ok',
            'filename' => $account->getFollowListFile(),
        ];
    }

    /**
     * Возвращает список id аккаунтов, которые не смогли авторизоваться
     *
     * @return array
     */
    public function actionGetBrokenAccountList()
    {
        $idList = Account::find()->select('id')->where('working = 0 AND banned = 0 AND (error_type IS NULL OR error_type = "unknown")')->asArray()->all();
        $brokenAccountList = [];

        foreach ($idList as $item) {
            $brokenAccountList[] = (int) $item['id'];
        }

        return [
            'status' => 'ok',
            'broken_account_list' => $brokenAccountList,
        ];
    }

    /**
     * Возвращает список id аккаунтов, которые сломаны
     *
     * @return array
     */
    public function actionGetUnworkingAccountList()
    {
        $idList = Account::find()->select('id')->where('working = 0 AND banned = 0')->asArray()->all();
        $brokenAccountList = [];

        foreach ($idList as $item) {
            $brokenAccountList[] = (int) $item['id'];
        }

        return [
            'status' => 'ok',
            'unworking_account_list' => $brokenAccountList,
        ];
    }

    /**
     * Возвращает список id аккаунтов, которые не имеют zaliv_name
     *
     * @return array
     */
    public function actionGetNotInfoAccountList()
    {
        $idList = Account::find()->select('id')->where('working = 1 AND zaliv_name IS NULL')->asArray()->all();

        $notInfoAccountList = [];

        foreach ($idList as $item) {
            $notInfoAccountList[] = (int) $item['id'];
        }

        return [
            'status' => 'ok',
            'not_info_account_list' => $notInfoAccountList,
        ];
    }

    /**
     * Количество аккаунтов
     *
     * @param $accountId
     * @param $working
     * @return array
     */
    public function actionSetWorking($accountId, $working)
    {
        $account = Account::findIdentity($accountId);
        $account->working = $working;
        $account->save();

        return [
            'status' => 'ok',
        ];
    }

    /**
     * Сохранить кол-во подписок
     *
     * @param $accountId
     * @param $followCount
     * @return array
     */
    public function actionSetFollowCount($accountId, $followCount)
    {
        $account = Account::findIdentity($accountId);
        $account->follow_count = $followCount;
        $account->save();

        return [
            'status' => 'ok',
        ];
    }

    /**
     * Проверяет, готов ли аккаунт к массфоллу
     *
     * @param $accountId
     * @return array
     */
    public function actionIsReadyForFollow($accountId)
    {
        $account = Account::findIdentity($accountId);

        $ready = false;

        if (!empty($account->zaliv_name) && !empty($account->following_count) && $account->following_count < 4000) {
            $ready = true;
        }

        return [
            'status' => 'ok',
            'ready' => $ready,
        ];
    }

    public function actionIsWorking($accountId)
    {
        $account = Account::findIdentity($accountId);

        if ($account->working === 1) {
            return [
                'status' => 'ok',
                'working' => true,
            ];
        }

        return [
            'status' => 'ok',
            'working' => false,
        ];
    }

    public function actionIsAvatar($accountId)
    {
        $account = Account::findIdentity($accountId);

        if ($account->avatar === 1) {
            return [
                'status' => 'ok',
                'avatar' => true,
            ];
        }

        return [
            'status' => 'ok',
            'avatar' => false,
        ];
    }

    public function actionIsPublic($accountId)
    {
        $account = Account::findIdentity($accountId);

        if ($account->public === 1) {
            return [
                'status' => 'ok',
                'public' => true,
            ];
        }

        return [
            'status' => 'ok',
            'public' => false,
        ];
    }

    public function actionIsFilled($accountId)
    {
        $account = Account::findIdentity($accountId);

        if ($account->filled === 1) {
            return [
                'status' => 'ok',
                'filled' => true,
            ];
        }

        return [
            'status' => 'ok',
            'filled' => false,
        ];
    }

    public function actionIsPhoto($accountId)
    {
        $account = Account::findIdentity($accountId);

        if ($account->photo === 1) {
            return [
                'status' => 'ok',
                'photo' => true,
            ];
        }

        return [
            'status' => 'ok',
            'photo' => false,
        ];
    }

    public function actionIsBiographed($accountId)
    {
        $account = Account::findIdentity($accountId);

        if ($account->biographed === 1) {
            return [
                'status' => 'ok',
                'biographed' => true,
            ];
        }

        return [
            'status' => 'ok',
            'biographed' => false,
        ];
    }

    public function actionIsCleared($accountId)
    {
        $account = Account::findIdentity($accountId);

        if ($account->cleared === 1) {
            return [
                'status' => 'ok',
                'cleared' => true,
            ];
        }

        return [
            'status' => 'ok',
            'cleared' => false,
        ];
    }

    public function actionSetClear($accountId)
    {
        $account = Account::findIdentity($accountId);
        $account->cleared = 1;
        $account->save();

        return [
            'status' => 'ok',
        ];
    }
}