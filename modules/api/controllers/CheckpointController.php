<?php

namespace app\modules\api\controllers;

use app\models\Instagram\Account;
use app\models\Instagram\checkpoint\CheckpointCaptcha;
use app\models\Instagram\checkpoint\CheckpointIncorrectPassword;
use app\models\Instagram\checkpoint\CheckpointMail;
use app\models\Instagram\checkpoint\Discover;
use app\models\Instagram\checkpoint\Exceptions\CheckpointException;
use app\models\Mail\Exceptions\MailLoginException;
use InstagramAPI\Exception\InstagramException;

class CheckpointController extends Controller
{
    /**
     * Починить аккаунт
     *
     * @param $accountId
     * @return array
     * @throws CheckpointException
     */
    public function actionFix($accountId)
    {
        $account = Account::findIdentity($accountId);

        if ($account->working || $account->banned) {
            return [
                'status' => 'ok',
            ];
        }

        if ($account->error_type == Discover::ERROR_SMS || $account->error_type == Discover::ERROR_SMS_GO_BACK || $account->error_type == Discover::ERROR_SMS_ADD_PHONE) {
            return [
                'status' => 'ok',
            ];
        }

        try {
            $account->deleteCookieFile();
            $account->prepare(false);

            $account->login(true);

            $account->error_type = null;
            $account->working = 1;
            $account->save();

            return [
                'status' => 'fail',
                'message' => 'Аккаунт не сломан'
            ];

        } catch (InstagramException $e) {

            try {
                $account->prepareCurl();

                $discover = new Discover($account);
                $checkpointType = $discover->identityCheckpointTypeByResponse($e->getResponse());

                switch ($checkpointType) {
                    case Discover::ERROR_EMAIL:

                        $checkpoint = new CheckpointMail($account, $discover->getHTML(), $discover->getCheckpointUrl(), false);
                        if ($checkpoint->doCheckpoint()) {

                            $account->error_type = null;
                            $account->working = 1;
                            $account->save();

                            return [
                                'status' => 'ok',
                            ];
                        } else {
                            return [
                                'status' => 'fail',
                                'message' => 'Email. Не смогли починить аккаунт',
                            ];
                        }

                    case Discover::ERROR_CAPTCHA:

                        $checkpoint = new CheckpointCaptcha($account, $discover->getHTML(), $discover->getCheckpointUrl(), false);
                        if ($checkpoint->doCheckpoint()) {

                            $account->error_type = null;
                            $account->working = 1;
                            $account->save();

                            return [
                                'status' => 'ok',
                            ];
                        } else {
                            return [
                                'status' => 'fail',
                                'message' => 'Капча. Не смогли починить аккаунт',
                            ];
                        }

                    case Discover::ERROR_INCORRECT_PASSWORD:

                        $checkpoint = new CheckpointIncorrectPassword($account, true);

                        if ($checkpoint->doCheckpoint()) {

                            $account->error_type = null;
                            $account->working = 1;
                            $account->save();

                            return [
                                'status' => 'ok',
                            ];
                        } else {
                            return [
                                'status' => 'fail',
                                'message' => 'Неверный пароль. Не смогли починить аккаунт',
                            ];
                        }

                    default:
                        throw new CheckpointException("Не смогли определить checkpoint по ошибке: " . $e->getMessage());
                }

            } catch (InstagramException $e) {
                return [
                    'status' => 'fail',
                    'message' => 'Не смогли починить аккаунт. ' . $e->getMessage(),
                ];
            } catch (CheckpointException $e) {
                return [
                    'status' => 'fail',
                    'message' => 'Не смогли починить аккаунт. ' . $e->getMessage(),
                ];
            } catch (MailLoginException $e) {
                return [
                    'status' => 'fail',
                    'message' => 'Не смогли починить аккаунт. ' . $e->getMessage(),
                ];
            }
        }
    }

    /**
     * Возвращает список id аккаунтов, которые требует подтверждения
     *
     * @return array
     */
    public function actionGetBrokenAccountList()
    {
        $idList = Account::find()->select('id')->where('working = 0 AND banned = 0 AND error_type != "' . Discover::ERROR_SMS_ADD_PHONE . '" AND error_type != "' . Discover::ERROR_SMS . '" AND error_type != "' . Discover::ERROR_SMS_GO_BACK .'"')->asArray()->all();
        $brokenAccountList = [];

        foreach ($idList as $item) {
            $brokenAccountList[] = (int) $item['id'];
        }

        return [
            'status' => 'ok',
            'broken_account_list' => $brokenAccountList,
        ];
    }
}