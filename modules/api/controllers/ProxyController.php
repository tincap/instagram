<?php

namespace app\modules\api\controllers;

use app\models\Instagram\Exceptions\MobileProxyException;
use app\models\Instagram\MobileProxy;

class ProxyController extends Controller
{
    /**
     * Обновить прокси
     *
     * @param $mobileProxyId
     * @return array
     */
    public function actionMobileProxyUpdate($mobileProxyId)
    {
        try {
            $mobileProxy = MobileProxy::findIdentity($mobileProxyId);

            $mobileProxy->updateProxy();

            return [
                'status' => 'ok',
            ];

        } catch (MobileProxyException $e) {
            return [
                'status' => 'fail',
                'message' => $e->getMessage(),
            ];
        }
    }

    /**
     * Кол-во мобильных прокси
     *
     * @return array
     */
    public function actionCountMobileProxy()
    {
        return [
            'status' => 'ok',
            'count' => MobileProxy::find()->count('id'),
        ];
    }
}