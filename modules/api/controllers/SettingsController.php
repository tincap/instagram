<?php

namespace app\modules\api\controllers;

use app\components\ImageResize;
use app\models\helpers\FileHelpers;
use app\models\helpers\PhotoHelpers;
use app\models\Instagram\Account;
use app\models\Instagram\checkpoint\Discover;
use InstagramAPI\Exception\InstagramException;

class SettingsController extends Controller
{
    /**
     * Сделать аккаунт публичным
     *
     * @param $accountId
     * @return array
     */
    public function actionPublic($accountId)
    {
        $account = Account::findIdentity($accountId);

        if ($account->public) {
            return [
                'status' => 'ok',
            ];
        }

        if (!$account->working || $account->banned) {
            return [
                'status' => 'fail',
                'message' => 'Аккаунт не работает'
            ];
        }

        try {
            $account->prepare(false);
            $account->login();

            sleep(1);

            $account->instagram->account->setPublic();

            $account->public = 1;
            $account->save();

            return [
                'status' => 'ok',
            ];
        } catch (InstagramException $e) {
            $discover = new Discover($account);

            $account->error_type = $discover->identityCheckpointTypeByResponse($e->getResponse());
            $account->working = 0;
            $account->save();

            return [
                'status' => 'fail',
                'message' => $account->error_type == null ? $e->getMessage() : $account->translateErrorType(),
            ];
        }
    }

    /**
     * Загрузить фотографию
     *
     * @param $accountId
     * @param $folderId
     * @return array
     */
    public function actionPhoto($accountId, $folderId)
    {
        $account = Account::findIdentity($accountId);
        $account->prepare(false);

        $success_upload = 0;

        if ($account->photo == 1) {
            return [
                'status' => 'ok',
                'success_upload' => $success_upload,
            ];
        }

        if ($account->working == 0) {
            return [
                'status' => 'fail',
                'message' => 'Аккаунт не работает',
                'success_upload' => $success_upload,
            ];
        }

        try {
            $account->login();

            $mediaCount = $account->getMediaCount();

            for ($i = $mediaCount + 1; $i <= \Yii::$app->params['photo_count']; $i++) {

                $photoPath = \Yii::getAlias("@app/data/instagram/photos/$folderId/" . $i . '.jpg');

                if (!file_exists($photoPath)) {
                    $photoPath = \Yii::getAlias("@app/data/instagram/photos/$folderId/" . $i . '.JPG');
                }

                if (!file_exists($photoPath)) {
                    return [
                        'status' => 'fail',
                        'message' => 'Не нашли фотографию',
                        'success_upload' => $success_upload,
                    ];
                }

                $imageResize = new ImageResize($photoPath);

                if ($imageResize->getDestWidth() > \Yii::$app->params['photo_size'] || $imageResize->getDestHeight() > \Yii::$app->params['photo_size']) {

                    if ($imageResize->getDestWidth() > $imageResize->getDestHeight()) {
                        $imageResize->resizeToWidth(\Yii::$app->params['photo_size']);
                        $imageResize->save($photoPath);
                    } else {
                        $imageResize->resizeToHeight(\Yii::$app->params['photo_size']);
                        $imageResize->save($photoPath);
                    }
                }

                if ($imageResize->getDestWidth() > $imageResize->getDestHeight()) {
                    PhotoHelpers::horizontal($photoPath);
                } else {
                    if ($imageResize->getDestWidth() < $imageResize->getDestHeight()) {
                        PhotoHelpers::vertical($photoPath);
                    }
                }

                $imageResize = null;

                $account->instagram->timeline->uploadPhoto($photoPath);
                $success_upload++;

                sleep(rand(20, 30));

            }
        } catch (InstagramException $e) {

            $discover = new Discover($account);

            $account->error_type = $discover->identityCheckpointTypeByResponse($e->getResponse());
            $account->working = 0;
            $account->save();

            return [
                'status' => 'fail',
                'message' => "Не смогли загрузить фотографию: " . $account->error_type == null ? $e->getMessage() : $account->translateErrorType(),
                'success_upload' => $success_upload,
            ];
        } catch (\Exception $e) {
            return [
                'status' => 'fail',
                'message' => "Не смогли загрузить фотографию: " . $e->getMessage(),
                'success_upload' => $success_upload,
            ];
        }

        $account->photo = 1;
        $account->save();

        return [
            'status' => 'ok',
            'success_upload' => $success_upload,
        ];
    }

    /**
     * Обновляет информацию о пользователе.
     *
     * @param $accountId
     * @return array
     */
    public function actionUpdateInfo($accountId)
    {
        $account = Account::findIdentity($accountId);

        if (!$account->working || $account->banned) {
            return [
                'status' => 'fail',
                'message' => 'Аккаунт не работает'
            ];
        }

        try {

            $account->prepare(false);
            $account->login();

            $response = $account->instagram->people->getSelfInfo();

            if ($response->hasUser()) {
                $account->media_count = $response->getUser()->getMediaCount();
                $account->follower_count = $response->getUser()->getFollowerCount();
                $account->following_count = $response->getUser()->getFollowingCount();

                // Верхнее описание
                if (strlen($response->getUser()->getBiography()) < 90) {
                    $account->biographed = 0;
                }
            }

            $response = $account->instagram->timeline->getSelfUserFeed();

            $zalivName = null;

            foreach ($response->getItems() as $item) {

                $account->zaliv_name = null;
                $account->save();

                if ($item->getCaption() == null || !$item->getCaption()->hasText()) {
                    return [
                        'status' => 'fail',
                        'message' => '- без заливки'
                    ];
                }

                preg_match('/^(@\w+)/iu', $item->getCaption()->getText(), $found);

                if (isset($found[1])) {
                    if ($zalivName != null) {
                        if ($zalivName != $found[1]) {
                            $account->zaliv_name = null;
                            $account->filled = 0;

                            return [
                                'status' => 'fail',
                                'message' => '- без заливки'
                            ];
                        }
                    } else {
                        $zalivName = $found[1];
                    }

                    $account->zaliv_name = $found[1];
                    $account->photo = 1;
                    $account->filled = 1;
                } else {

                    if (preg_match('/(@\w+)/iu', $item->getCaption()->getText(), $found)) {
                        $newCaption = preg_replace('/"м/iu', '', $item->getCaption()->getText());
                        $account->instagram->media->edit($item->getId(), $newCaption);
                        $zalivName = $found[1];

                        $account->zaliv_name = $zalivName;
                        sleep(1);

                    } else {
                        $account->zaliv_name = null;
                        $account->filled = 0;
                    }

                    $account->zaliv_name = null;
                    $account->filled = 0;
                }
            }

            $account->save();

            if ($account->zaliv_name != null) {
                return [
                    'status' => 'ok',
                    'zaliv_name' => $account->zaliv_name,
                ];
            }

            return [
                'status' => 'fail',
                'message' => '- без заливки'
            ];
        } catch (InstagramException $e) {

            $discover = new Discover($account);

            $account->error_type = $discover->identityCheckpointTypeByResponse($e->getResponse());
            $account->working = 0;
            $account->save();

            return [
                'status' => 'fail',
                'message' => $account->error_type == null ? $e->getMessage() : $account->translateErrorType(),
            ];
        }
    }

    /**
     * Загружает аватарку
     *
     * @param $accountId
     * @return array
     */
    public function actionAvatar($accountId)
    {
        $account = Account::findIdentity($accountId);

        if (!$account->working) {
            return [
                'status' => 'fail',
                'message' => 'Аккаунт не работает',
            ];
        }

        if ($account->avatar == 1) {
            return [
                'status' => 'ok',
            ];
        }

        try {
            $account->prepare(false);
            $account->login();

            $countDir = FileHelpers::countDir(\Yii::getAlias("@app/data/Instagram/photos"));

            if ($countDir < 5) {
                return [
                    'status' => 'fail',
                    'message' => 'Слишком мало папок в @app/data/Instagram/photos',
                ];
            }

            $folderId = rand(1, FileHelpers::countDir(\Yii::getAlias("@app/data/Instagram/photos")));
            $photoId = rand(1, 3);

            $photoPath = \Yii::getAlias("@app/data/Instagram/photos/{$folderId}/{$photoId}.jpg");

            if (!file_exists($photoPath)) {
                return [
                    'status' => 'fail',
                    'message' => 'Не нашли файл ' . $photoPath,
                ];
            }


            $account->instagram->account->changeProfilePicture($photoPath);

            $account->avatar = 1;
            $account->save();

            return [
                'status' => 'ok'
            ];

        } catch (InstagramException $e) {
            $discover = new Discover($account);

            $account->error_type = $discover->identityCheckpointTypeByResponse($e->getResponse());
            $account->working = 0;
            $account->save();

            return [
                'status' => 'fail',
                'message' => $account->error_type == null ? $e->getMessage() : $account->translateErrorType(),
            ];
        }
    }

    /**
     * Изменить профиль
     *
     * @param $accountId
     * @return array
     */
    public function actionBiography($accountId)
    {
        $account = Account::findIdentity($accountId);

        if (!$account->working) {
            return [
                'status' => 'fail',
                'message' => 'Аккаунт не работает',
            ];
        }

        if ($account->biographed == 1) {
            return [
                'status' => 'ok',
            ];
        }

        try {
            $account->prepare(false);
            $account->login();

            $firstnamePath = \Yii::getAlias('@app/data/files/custom/firstnameList.txt');

            do {
                $firstname = FileHelpers::getFirstStrByFile($firstnamePath);
            } while (strlen($firstname) > 30);

            if (empty($firstname)) {
                return [
                    'status' => 'fail',
                    'message' => 'firstnameList.txt пустой',
                ];
            }
            $biographyPath = \Yii::getAlias('@app/data/files/custom/biographyList.txt');

            do {
                $biography = FileHelpers::getElementByFile(FileHelpers::BORDER, $biographyPath);
            } while (strlen($biography) > 150);

            if (empty($biography)) {
                return [
                    'status' => 'fail',
                    'message' => 'biography.txt пустой',
                ];
            }

            $account->instagram->account->editProfile(
                '',
                '',
                $firstname,
                $biography,
                $account->mail->username,
                2
            );

            $account->biographed = 1;
            $account->save();

            return [
                'status' => 'ok',
            ];

        } catch (InstagramException $e) {
            $discover = new Discover($account);

            $account->error_type = $discover->identityCheckpointTypeByResponse($e->getResponse());
            $account->working = 0;
            $account->save();

            return [
                'status' => 'fail',
                'message' => $account->error_type == null ? $e->getMessage() : $account->translateErrorType(),
            ];
        } catch (\Exception $e) {
            return [
                'status' => 'fail',
                'message' => "Не смогли загрузить описание: " . $e->getMessage(),
            ];
        }
    }

    /**
     * Заполнение фотографий
     *
     * @param $accountId
     * @return array
     */
    public function actionFill($accountId)
    {
        $account = Account::findIdentity($accountId);

        if (!$account->working) {
            return [
                'status' => 'fail',
                'message' => 'Аккаунт не работает',
            ];
        }

        if ($account->filled) {
            return [
                'status' => 'ok',
            ];
        }

        if (!$account->photo) {
            return [
                'status' => 'fail',
                'message' => 'Не загружены фотки на аккаунт',
            ];
        }

        try {
            $account->prepare(false);
            $account->login();

            $response = $account->instagram->timeline->getSelfUserFeed();

            $mediaCount = count($response->getItems());

            if ($mediaCount > 6) {

                $account->cleared = 0;
                $account->photo = 0;
                $account->filled = 0;
                $account->save();

                return [
                    'status' => 'fail',
                    'message' => 'Количество фоток больше 6-х',
                ];
            }

            if ($mediaCount < 6) {

                $account->photo = 0;
                $account->filled = 0;
                $account->save();

                return [
                    'status' => 'fail',
                    'message' => 'Количество фоток меньше 6-х',
                ];
            }

            $photoCaption = FileHelpers::getElementByFile(FileHelpers::BORDER, \Yii::getAlias('@app/data/files/custom/photoCaptions.txt'));

            if (empty($photoCaption)) {
                return [
                    'status' => 'fail',
                    'message' => 'photoCaption is empty',
                ];
            }

            foreach ($response->getItems() as $item) {
                $account->instagram->media->edit($item->getId(), $photoCaption);
                sleep(rand(5, 10));
            }

            $account->filled = 1;
            $account->save();

            return [
                'status' => 'ok',
            ];

        } catch (InstagramException $e) {
            $discover = new Discover($account);

            $account->error_type = $discover->identityCheckpointTypeByResponse($e->getResponse());
            $account->working = 0;
            $account->save();

            return [
                'status' => 'fail',
                'message' => $account->error_type == null ? $e->getMessage() : $account->translateErrorType(),
            ];
        }
    }
}