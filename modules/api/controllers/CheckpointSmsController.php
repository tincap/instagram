<?php

namespace app\modules\api\controllers;

use app\models\Instagram\Account;
use app\models\Instagram\checkpoint\Discover;
use app\models\Instagram\checkpoint\Exceptions\CheckpointException;
use app\models\Instagram\checkpoint\models\CheckpointSmsModel;
use app\models\Instagram\SmsServices\BasicSmsServiceModel;
use \app\models\Instagram\SmsServices\Exceptions\SmsServiceException;

class CheckpointSmsController extends Controller
{
    /**
     * Чинит аккаунт
     *
     * @param $accountId
     * @param $phoneId
     * @param $phoneNumber
     * @param $smsService
     * @param int $smsNumber
     * @return array
     */
    public function actionFix($accountId, $phoneId, $phoneNumber, $smsService, $smsNumber = 1)
    {
        if ($phoneId == null || $phoneNumber == null || $smsService == null) {
            return [
                'status' => 'ok',
                'message' => 'Empty params',
            ];
        }

        try {
            CheckpointSmsModel::fix(false, $accountId, $phoneId, $phoneNumber, $smsService, $smsNumber);

            $account = Account::findIdentity($accountId);
            $account->working = 1;
            $account->error_type = null;
            $account->save();

            return [
                'status' => 'ok',
            ];
        } catch (SmsServiceException $e) {
            return [
                'status' => 'fail',
                'message' => $e->getMessage(),
            ];
        } catch (CheckpointException $e) {
            return [
                'status' => 'fail',
                'message' => $e->getMessage(),
            ];
        } catch (\Exception $e) {
            return [
                'status' => 'fail',
                'message' => $e->getMessage() . ' in ' . $e->getFile() . ' ' .  $e->getLine(),
            ];
        }
    }

    /**
     * Получить доступный номер
     *
     * @param $smsService
     * @param int $country
     * @return array
     */
    public function actionGetNumber($smsService, $country = 0)
    {
        $smsServiceModel = new BasicSmsServiceModel(false, null, 1, $smsService, $country);

        try {
            $phone = $smsServiceModel->getNumber();

            if (is_array($phone)) {

                return [
                    'status' => 'ok',
                    'phone_id' => (string) $phone['id'],
                    'phone_number' => (string) $phone['number'],
                    'sms_service' => $smsServiceModel->getSmsService(),
                ];

            } else {
                throw new SmsServiceException("Не смогли получить номер");
            }

        } catch (\Exception $e) {
            return [
                'status' => 'fail',
                'message' => $e->getMessage(),
            ];
        }
    }

    /**
     * Возвращает список id аккаунтов, которые требует подтверждения
     *
     * @return array
     */
    public function actionGetBrokenAccountList()
    {
        $idList = Account::find()->select('id')->where('error_type = "' . Discover::ERROR_SMS_ADD_PHONE . '" or error_type = "' . Discover::ERROR_SMS . '" or error_type = "' . Discover::ERROR_SMS_GO_BACK .'"')->asArray()->all();

        $brokenAccountList = [];

        foreach ($idList as $item) {
            $brokenAccountList[] = (int) $item['id'];
        }

        return [
            'status' => 'ok',
            'broken_account_list' => $brokenAccountList,
        ];
    }
}