<?php

namespace app\modules\api\controllers;

use app\models\Instagram\Account;
use app\models\Instagram\checkpoint\Discover;
use InstagramAPI\Exception\InstagramException;

class Controller extends \yii\web\Controller
{
    public $enableCsrfValidation = false;

    /**
     * Обработка ошибок типа InstagramException
     *
     * @param InstagramException $e
     * @param Account $account
     * @return array
     */
    public function catchErrors(InstagramException $e, Account $account)

        {$discover = new Discover($account);
        $account->error_type = $discover->identityCheckpointTypeByResponse($e->getResponse());

        if ($discover->isBanned()) {
            $account->banned = 1;
        }

        $account->working = 0;
        $account->save();

        return [
            'status' => 'fail',
            'message' => $account->error_type == null ? $e->getMessage() : $account->translateErrorType(),
        ];
    }
}