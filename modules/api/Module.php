<?php

namespace app\modules\api;

use Yii;
use yii\web\Response;

class Module extends \yii\base\Module
{
    public function init()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $this->layout = false;

        parent::init();
    }
}